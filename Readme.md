# Libraries:

Referenced UnityEngine.dll, UnityEditor.dll and Sirenix.OdinInspector.Attributes.dll are removed from repository! Use your own while rebuilding solution.

* **[Managers](https://bitbucket.org/Xamtos/libraries/src/master/Projects/Managers/).** 
Library containing managers:
    - Logger.
	- ServiceLocator.
	- EventAggregator.
	
* **[Utilities](https://bitbucket.org/Xamtos/libraries/src/master/Projects/Utilities/).**
Various wrappers, extensions, attributes, generic classes.