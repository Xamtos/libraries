﻿Register any class via
    
    Services.Register(class);
    
Access registered class via

    Services.Get<Type>();
    
Exception is thrown if service not found.