﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Managers;

/// <summary>
/// Static Services class used to register and access registered classes.
/// </summary>
public static class Services
{
    private static readonly List<object> RegisteredServices = new List<object>();
    
    // Used for performant lookup through requested Type's interfaces.
    private static readonly Dictionary<Type, Type[]> ServiceInterfaces = new Dictionary<Type, Type[]>();

    private static readonly Dictionary<Type, object> CachedServices = new Dictionary<Type, object>();
    
    /// <summary> Register new class in Services or replace existing one. </summary>
    /// <param name="service">Class to be registered.</param>
    /// <exception cref="ArgumentNullException"></exception>
    public static void Register<T>(T service) where T : class
    {
        if (service == null) throw new ArgumentNullException(nameof(service));

        Type newServiceType = service.GetType();
        var newServiceInterfaces = newServiceType.GetInterfaces();
        
        int existingServiceIndex = !newServiceInterfaces.Any()
            ? RegisteredServices.FindIndex(a => a.GetType() == newServiceType)
            : RegisteredServices.FindIndex(a =>
            {
                var savedInterfaces = SavedInterfacesOfType(a);
                return savedInterfaces.Intersect(newServiceInterfaces).Count() == Math.Max(newServiceInterfaces.Length, savedInterfaces.Length);
            });

        if (existingServiceIndex >= 0)
            RegisteredServices[existingServiceIndex] = service;
        else
            RegisteredServices.Add(service);
        
        ServiceInterfaces[newServiceType] = newServiceInterfaces;
        CleanupCachedServices(newServiceInterfaces);
    }

    private static void CleanupCachedServices(Type[] newServiceInterfaces)
    {
        foreach (var serviceInterface in newServiceInterfaces)
            if (CachedServices.ContainsKey(serviceInterface))
                CachedServices.Remove(serviceInterface);
    }

    /// <summary>
    /// Find registered class by type.
    /// </summary>
    /// <exception cref="KeyNotFoundException"></exception>
    public static T Get<T>() where T : class
        => TryGet<T>() ?? throw new KeyNotFoundException($"Type not present in registered services: {typeof(T)}");

    /// <summary>
    /// Check if class of type is registered.
    /// </summary>
    public static bool Contains<T>() where T : class
        => TryGet<T>() != null;
    
    public static bool TryGet<T>(out T result) where T : class
    {
        var type = typeof(T);
        if (CachedServices.TryGetValue(type, out var tResult))
        {
            result = (T)tResult;
            return true;
        }
        
        result = TryGet<T>();
        if (result == null) return false;

        CachedServices[type] = result;
        return true;
    }

    /// <summary>
    /// Remove all classes from Services.
    /// </summary>
    public static void Clear()
    {
        RegisteredServices.Clear();
        ServiceInterfaces.Clear();
    }

    private static T TryGet<T>() where T : class
    {
        return !typeof(T).IsInterface
            // Find service by type.
            ? RegisteredServices.Find(s => s.GetType() == typeof(T)) as T
            // Find service by interface.
            : RegisteredServices.Find(s => SavedInterfacesOfType(s).Contains(typeof(T))) as T;
    }

    private static Type[] SavedInterfacesOfType(object o)
        => ServiceInterfaces.TryGetValue(o.GetType(), out var interfaces) ? interfaces : Array.Empty<Type>();
}