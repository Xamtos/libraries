﻿namespace Managers.Logger;

/// <summary>
/// Interface that represents logger class.
/// </summary>
public interface ILog
{
    /// <summary> Send message using current logger. </summary>
    /// <param name="message">Message to send.</param>
    /// <param name="context">Context added to message.</param>
    /// <param name="logArea">Messages logged for specified log area only. Use <c>null</c> to log all messages.</param>
    public void Log(object message, object context = null, string logArea = null);

    public void Warning(object message, object context = null, string logArea = null);
    public void Error(object message, object context = null);
    public void Debug(object message, object context = null, string logArea = null);
}