﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Managers.Logger.Loggers;

internal class Logger : ILog
{
    private readonly string area;
    private readonly int level;
    private readonly Dictionary<int, Action<object>> actions = new();
    private readonly StringBuilder builder = new();
    private readonly bool includeAreaInLogs;

    internal Logger((Action<object>, int)[] logActions, int minLevel = 0, string area = null, bool includeAreaInLogs = true)
    {
        foreach (var action in logActions) actions[action.Item2] = action.Item1;
            
        level = minLevel;
        this.area = area ?? string.Empty;
        this.includeAreaInLogs = includeAreaInLogs;
    }
        
    public void Log(object message, object context = null, string logArea = null)
        => LogInternal(message, context, (int)LogLevel.Info, logArea);

    public void Warning(object message, object context = null, string logArea = null)
        => LogInternal(message, context, (int)LogLevel.Warning, logArea);

    public void Error(object message, object context = null)
        => LogInternal(message, context, (int)LogLevel.Error);

    public void Debug(object message, object context = null, string logArea = null)
        => LogInternal(message, context, (int)LogLevel.Debug, logArea);
    
    private void LogInternal(object message, object context = null, int logLevel = 0, string logArea = null)
    {
        if (!IsCorrectLevel(logLevel)) return;
        if (!IsCorrectArea(logArea)) return;

        builder.Clear();
        if (context != null) builder.Append($"[{context}] ");
        if (includeAreaInLogs && !string.IsNullOrEmpty(logArea)) builder.Append($"[{logArea}] ");
        builder.Append(message);
        actions[logLevel].Invoke(builder.ToString());
    }

    private bool IsCorrectLevel(int logLevel)
        => logLevel >= level;

    private bool IsCorrectArea(string logArea)
        => string.IsNullOrEmpty(area) || string.IsNullOrEmpty(logArea) || area.Equals(logArea);
}