﻿using System.Diagnostics.CodeAnalysis;

namespace Managers.Logger.Loggers;

[ExcludeFromCodeCoverage]
internal class EmptyLogger : ILog
{
    public void Log(object message, object context = null, string logArea = null) { }
    public void Warning(object message, object context = null, string logArea = null) { }
    public void Error(object message, object context = null) { }
    public void Debug(object message, object context = null, string logArea = null) { }
    public void Assert(bool assert, object message) { }
}