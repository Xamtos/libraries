﻿using System;
using System.Linq;
using Managers.Logger.Loggers;

namespace Managers.Logger;

/// <summary>
/// Logger creating class.
/// </summary>
public static class LoggerBuilder
{
    /// <summary>
    /// Create empty logger that does nothing.
    /// </summary>
    public static ILog CreateEmpty() => new EmptyLogger();
        
    /// <summary>
    /// Create custom logger.
    /// </summary>
    public static ILog Create((Action<object>, LogLevel)[] logActions, LogLevel level = LogLevel.Info, string area = null, bool includeAreaInLogs = true)
    {
        if (!Enum.IsDefined(typeof(LogLevel), level)) throw new ArgumentOutOfRangeException(nameof(level));
        if (logActions == null) throw new ArgumentNullException(nameof(logActions));

        int logLevel = -1;
        while (Enum.IsDefined(typeof(LogLevel), ++logLevel))
        {
            if ((int)level > logLevel) continue;
            if (logActions.All(a => (int)a.Item2 != logLevel)) throw new ArgumentOutOfRangeException(nameof(logActions));
        }
            

        return new Loggers.Logger(logActions.Select(a => (a.Item1, (int)a.Item2)).ToArray(), (int)level, area, includeAreaInLogs);
    }
}