﻿namespace Managers.EventAggregator;

public interface IListener<in T> where T : IMessage
{
    void OnTrigger(T message);
}