using Managers.Logger;

namespace Managers.EventAggregator;

internal class EventAggregatorPollLogged : EventAggregatorPoll
{
    private readonly ILog logger;

    internal EventAggregatorPollLogged(ILog logger)
    {
        this.logger = logger;
        logger.Debug("Logged EventAggregator created.", this, "Events");
    }

    public override void Subscribe<T>(IListener<T> listener)
    {
        base.Subscribe(listener);
        logger.Debug($"Subscribed to incident: {typeof(T)}", this, "Events");
    }
        
    public override void Unsubscribe<T>(IListener<T> listener)
    {
        base.Unsubscribe(listener);
        logger.Debug($"Unsubscribed from incident: {typeof(T)}", this, "Events");
    }

    public override void UnsubscribeAll<T>()
    {
        base.UnsubscribeAll<T>();
        logger.Debug($"Unsubscribed all from incident: {typeof(T)}", this, "Events");
    }

    public override void Send<T>(T args)
    {
        logger.Debug($"Received incident: {typeof(T)}", this, "Events");
        base.Send(args);
    }

    public override void PollEvents()
    {
        logger.Debug($"Starting polling incidents...", this, "Events");
        base.PollEvents();
    }
    
    public override string ToString()
        => nameof(EventAggregator);
}