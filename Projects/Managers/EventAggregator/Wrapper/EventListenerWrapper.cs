using System.Collections.Generic;

namespace Managers.EventAggregator.Wrapper
{
    internal class EventListenerWrapper<T> : IEventListenerWrapper where T : struct, IMessage
    {
        public List<IListener<T>> Listeners { get; } = [];
        public Queue<T> Messages { get; } = new();
        public Lock Locker { get; } = new();
        public void ProcessQueue()
        {
            while (Messages.Count > 0)
            {
                var message = Messages.Dequeue();
                Locker.IsLocked = true;
                foreach (var listener in Listeners) listener.OnTrigger(message);
                Locker.IsLocked = false;
            }
        }
    
        public void Dispose()
        {
            Locker.Dispose();
            Listeners.Clear();
            Messages.Clear();
        }
    }
}