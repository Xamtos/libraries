using System;

namespace Managers.EventAggregator.Wrapper;

internal interface IEventListenerWrapper : IDisposable
{
    void ProcessQueue();
}