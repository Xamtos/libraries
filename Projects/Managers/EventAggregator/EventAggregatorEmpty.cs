﻿using System.Diagnostics.CodeAnalysis;

namespace Managers.EventAggregator;

[ExcludeFromCodeCoverage]
internal class EventAggregatorEmpty : IMessenger
{
    public void Subscribe<T>(IListener<T> listener) where T : struct, IMessage { }
    public void Unsubscribe<T>(IListener<T> listener) where T : struct, IMessage { }
    public void UnsubscribeAll<T>() where T : struct, IMessage { }
    public void Send<T>(T args) where T : struct, IMessage { }
    public void Dispose() { }
}