﻿using Managers.Logger;

namespace Managers.EventAggregator;

internal class EventAggregatorLogged : EventAggregator
{
    private readonly ILog logger;

    internal EventAggregatorLogged(ILog logger)
    {
        this.logger = logger;
        logger.Debug("Logged EventAggregator created.", this, "Events");
    }

    public override void Subscribe<T>(IListener<T> listener)
    {
        base.Subscribe(listener);
        logger.Debug($"Subscribed to incident: {typeof(T)}", this, "Events");
    }
        
    public override void Unsubscribe<T>(IListener<T> listener)
    {
        base.Unsubscribe(listener);
        logger.Debug($"Unsubscribed from incident: {typeof(T)}", this, "Events");
    }

    public override void UnsubscribeAll<T>()
    {
        base.UnsubscribeAll<T>();
        logger.Debug($"Unsubscribed all from incident: {typeof(T)}", this, "Events");
    }

    public override void Send<T>(T args)
    {
        logger.Debug($"Sent incident: {typeof(T)}", this, "Events");
        base.Send(args);
    }

    public override string ToString()
        => nameof(EventAggregator);
}