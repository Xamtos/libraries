﻿using System;

namespace Managers.EventAggregator;

/// <summary>
/// Interface providing event subscription/triggering.
/// </summary>
public interface IMessenger : IDisposable, IMessageReceiver
{
    /// <summary> Subscribe to provided event type. All subscribed actions are invoked once event is triggered. </summary>
    /// <exception cref="ArgumentException"></exception>
    void Subscribe<T>(IListener<T> listener) where T: struct, IMessage;

    /// <summary> Remove from subscription on provided event type. </summary>
    /// <exception cref="ArgumentException"></exception>
    void Unsubscribe<T>(IListener<T> listener) where T: struct, IMessage;

    /// <summary>
    /// Remove all subscriptions from provided event type.
    /// </summary>
    void UnsubscribeAll<T>() where T: struct, IMessage;
}

public interface IMessageReceiver
{
    /// <summary>Invoke all subscribed events to provided event type.</summary>
    /// <param name="args">Data passed to each subscriber.</param>
    void Send<T>(T args) where T: struct, IMessage;
}