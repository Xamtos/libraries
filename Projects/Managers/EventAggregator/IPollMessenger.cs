namespace Managers.EventAggregator;

public interface IPollMessenger : IMessenger
{
    void PollEvents();
}