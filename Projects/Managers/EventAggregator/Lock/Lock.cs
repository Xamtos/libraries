using System;

namespace Managers.EventAggregator;

internal class Lock : IDisposable
{
    public event Action ScheduledAfterLock;
    private bool isLocked;
    public bool IsLocked
    {
        get => isLocked;
        set
        {
            if (isLocked == value) throw new InvalidOperationException("Duplicate lock operation detected");
            isLocked = value;
            if (value == false)
            {
                ScheduledAfterLock?.Invoke();
                ScheduledAfterLock = null;
            }
        }
    }
        
    public void Dispose()
        => ScheduledAfterLock = null;
}