﻿using System;
using System.Collections.Generic;
using Managers.EventAggregator.Wrapper;

namespace Managers.EventAggregator;

internal class EventAggregator : IMessenger
{
    private readonly Dictionary<int, IEventListenerWrapper> wrappers = new ();
        
    public virtual void Subscribe<T>(IListener<T> listener) where T : struct, IMessage
    { 
        if (listener == null) throw new ArgumentNullException(nameof(listener));

        var typeId = typeof(T).GetHashCode();
        if (!wrappers.TryGetValue(typeId, out var info))
        {
            var newWrapper = new EventListenerWrapper<T>();
            newWrapper.Listeners.Add(listener);
            wrappers[typeId] = newWrapper;
            return;
        }

        var wrapper = (EventListenerWrapper<T>)info;
        if (wrapper.Locker.IsLocked) wrapper.Locker.ScheduledAfterLock += () => wrapper.Listeners.Add(listener);
        else wrapper.Listeners.Add(listener);
    }

    public virtual void Unsubscribe<T>(IListener<T> listener) where T : struct, IMessage
    {
        if (listener == null) throw new ArgumentNullException(nameof(listener));

        var typeId = typeof(T).GetHashCode();
        if (!wrappers.TryGetValue(typeId, out var info)) return;

        var wrapper = (EventListenerWrapper<T>)info;
        if (wrapper.Locker.IsLocked) wrapper.Locker.ScheduledAfterLock += () => wrapper.Listeners.Remove(listener);
        else wrapper.Listeners.Remove(listener);
    }

    public virtual void UnsubscribeAll<T>() where T : struct, IMessage
    {
        var typeId = typeof(T).GetHashCode();
        if (!wrappers.TryGetValue(typeId, out var info)) return;

        var wrapper = (EventListenerWrapper<T>)info;
        if (wrapper.Locker.IsLocked) wrapper.Locker.ScheduledAfterLock += wrapper.Listeners.Clear;
        else wrapper.Listeners.Clear();
    }

    public virtual void Send<T>(T args) where T : struct, IMessage
    {
        var typeId = typeof(T).GetHashCode();
        if (!wrappers.TryGetValue(typeId, out var info)) return;

        var wrapper = (EventListenerWrapper<T>)info;
        wrapper.Messages.Enqueue(args);
        wrapper.ProcessQueue();
    }

    public void Dispose()
    {
        foreach (var kvp in wrappers) kvp.Value.Dispose();
    }
}