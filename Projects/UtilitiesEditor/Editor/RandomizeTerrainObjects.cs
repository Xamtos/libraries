﻿using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace UtilitiesEditor;

/// <summary>
/// Set rotation of terrain objects with specified name to randomized angle
/// </summary>
public class RandomizeTerrainObjects : OdinEditorWindow
{
    [SerializeField] private string objectName = "Grass";
    [SerializeField] private float minAngle = -60;
    [SerializeField] private float maxAngle = 60;
        
    private float MinAngleRads => Mathf.Deg2Rad * minAngle;
    private float MaxAngleRads => Mathf.Deg2Rad * maxAngle;
        
    [MenuItem("Tools/Randomize Terrain Objects")]
    public static void ShowWindow()
        => GetWindow<RandomizeTerrainObjects>();

    [Button]
    private void Execute()
    {
        var terrain = ((GameObject)Selection.objects[0]).GetComponent<Terrain>();
        var grassIndex = terrain.terrainData.treePrototypes.ToList().FindIndex(a => a.prefab.name == objectName);
        var treeInstances = terrain.terrainData.treeInstances.ToList();
        for (var i = 0; i < treeInstances.Count; i++)
        {
            var treeInstance = treeInstances[i];
            if (treeInstance.prototypeIndex != grassIndex) continue;
            treeInstance.rotation = Random.Range(MinAngleRads, MaxAngleRads);
            terrain.terrainData.SetTreeInstance(i, treeInstance);
        }
    }
}