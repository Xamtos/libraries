﻿using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace UtilitiesEditor;

public class RenameMultiple : OdinEditorWindow
{
    [SerializeField] private string toReplace;
    [SerializeField] private string newString;

    [MenuItem("Tools/Rename multiple files")]
    public static void ShowWindow()
        => GetWindow<RenameMultiple>();

    [Button]
    public void Rename()
    {
        var objects = Selection.objects;
        foreach (var obj in objects)
        {
            var path = AssetDatabase.GetAssetPath(obj);
            AssetDatabase.RenameAsset(path, obj.name.Replace(toReplace, newString));
        }
    }
}