﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UtilitiesEditor;

public abstract class SimpleBuildScript : OdinEditorWindow
{
    private IReadOnlyList<string> FilesToDelete => new[]
    {
        $"{PlayerSettings.productName}.exe", 
        "UnityCrashHandler64.exe", 
        "UnityCrashHandler32.exe", 
        "UnityPlayer.dll", 
        "WinPixEventRuntime.dll", 
        $"{PlayerSettings.productName}_Data", 
        $"{PlayerSettings.productName}_BackUpThisFolder_ButDontShipItWithYourGame", 
        "MonoBleedingEdge",
        "GameAssembly.dll",
        "WinPixEventRuntime.dll",
        "baselib.dll"
    };
        
    [FolderPath(AbsolutePath = true)]
    [OnInspectorInit("@PlayerPrefs.GetString(\"SimpleBuildPath\")")]
    public string BuildPath
    {
        get => PlayerPrefs.GetString("SimpleBuildPath", "");
        set => PlayerPrefs.SetString("SimpleBuildPath", value);
    }
        
    [ShowInInspector]
    [OnInspectorInit("@PlayerPrefs.GetString(\"SimpleBuildDefines\")")]
    public string Defines
    {
        get => PlayerPrefs.GetString("SimpleBuildDefines", "");
        set => PlayerPrefs.SetString("SimpleBuildDefines", value);
    }

    [Button]
    private void Build()
    {
        if (string.IsNullOrEmpty(BuildPath)) return;
            
        ClearFolder();
        var oldDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, Defines);
        var scenes = GetScenes();
        LaunchBuild(scenes);
        PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, oldDefines);
    }

    private void ClearFolder()
    {
        if (!Directory.Exists(BuildPath)) return;

        foreach (var file in Directory.GetFiles(BuildPath))
            if (FilesToDelete.Contains(Path.GetFileName(file)))
                File.Delete(file);

        foreach (var directory in Directory.GetDirectories(BuildPath))
            if (FilesToDelete.Contains(Path.GetFileName(directory)))
                Directory.Delete(directory, true);
    }

    private static string[] GetScenes()
    {
        var strings = new []
        {
            SceneManager.GetSceneByBuildIndex(0).path
        };
        return strings;
    }


    private void LaunchBuild(string[] scenes)
    {
        AdditionalBuildSteps();
        BuildReport report = BuildPipeline.BuildPlayer(scenes, Path.Combine(BuildPath, $"{PlayerSettings.productName}.exe"), EditorUserBuildSettings.selectedStandaloneTarget, BuildOptions.Development);
        ReportErrors(report);
    }

    private static void ReportErrors(BuildReport report)
    {
        if (report.summary.totalErrors <= 0) return;

        foreach (var step in report.steps)
        foreach (var message in step.messages)
        {
            switch (message.type)
            {
                case LogType.Error:
                    Debug.LogError($"Error: {message.content}");
                    break;
                case LogType.Exception:
                    Debug.LogError($"Exception: {message.content}");
                    break;
            }
        }

        Debug.Log("Total errors = " + report.summary.totalErrors);
    }

    protected abstract void AdditionalBuildSteps();
}