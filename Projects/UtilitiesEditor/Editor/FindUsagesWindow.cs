﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using ReadonlyField = UtilitiesEditor.FindUsagesUtilities.ReadonlyField;

namespace UtilitiesEditor;

public class FindUsagesWindow : OdinEditorWindow
{
    [ShowInInspector, ReadOnly, ShowIf("@Scanning != string.Empty"), PropertyOrder(0)]
    private string Scanning => pathsToScan.FirstOrDefault().Key ?? string.Empty;
        
    [ShowInInspector, ShowIf("@Scanning != string.Empty"), ProgressBar(0, 100), PropertyOrder(1)] 
    private float Loading => Mathf.RoundToInt((assetsCount.total - assetsCount.remaining) * 100f / assetsCount.total);

    [ReadOnly, ShowInInspector, PropertyOrder(2)] 
    private Object Target { get; set; }
        
    [Space, ListDrawerSettings(IsReadOnly = true), ShowInInspector, PropertyOrder(3)] 
    private List<ReadonlyField> usages = new List<ReadonlyField>();

    private ConcurrentDictionary<string, string> pathsToScan = new ConcurrentDictionary<string, string>();
    private readonly ConcurrentQueue<string> asyncAssetsQueue = new ConcurrentQueue<string>();
    private (int remaining, int total) assetsCount = (0, 1);

    protected virtual string[] GetAdditionalStringsToCheck(string targetName, string targetGuid) => Array.Empty<string>();
    private bool IsUsed(string targetGuid, string[] additionalStrings, string assetText)
    {
        if (assetText.Contains(targetGuid)) return true;

        foreach (var additionalString in additionalStrings)
            if (assetText.Contains(additionalString))
                return true;

        return false;
    }

    protected void Init()
    {
        usages.Clear();
        var paths = FindUsagesUtilities.CollectObjectPaths();
        pathsToScan = new ConcurrentDictionary<string, string>(paths.ToDictionary(a => a, b => b));
            
        Target = Selection.objects[0];
        if (Target == null)
        {
            Debug.LogError("Нужно выбрать объект.");
            return;
        }
            
        var targetGuid = AssetDatabase.GUIDFromAssetPath(AssetDatabase.GetAssetPath(Target)).ToString();
        assetsCount = (paths.Length, paths.Length);

        var additional = GetAdditionalStringsToCheck(Target.name, targetGuid);
        Task.Run(() => Parallel.ForEach(paths, a =>
        {
            var text = File.ReadAllText(a);
            if (IsUsed(targetGuid, additional, text))
                asyncAssetsQueue.Enqueue(a);

            assetsCount.remaining--;
            pathsToScan.TryRemove(a, out _);
        }));
    }


    private void Update()
    {
        if (asyncAssetsQueue.Count == 0 || !asyncAssetsQueue.TryDequeue(out var queued)) return;

        usages.Add(new ReadonlyField {Value = AssetDatabase.LoadAssetAtPath<Object>(queued)});
    }

    private void OnInspectorUpdate()
        => Repaint();
        
    [MenuItem("Tools/Find Usages", priority = 0)]
    public static void ShowWindow()
    {
        GetWindow<FindUsagesWindow>().Init();
    }
        
    [Button, PropertyOrder(4)]
    private void SelectAll()
        => Selection.objects = usages.Select(a => a.Value).ToArray();
}

public static class FindUsagesUtilities
{
    private static readonly string[] ForbiddenExtensions = {".fbx"};
    private static readonly string AssetsFilter = "t:ScriptableObject t:GameObject t:Scene t:Material";
        
    internal static string[] CollectObjectPaths()
    {
        return AssetDatabase.FindAssets(AssetsFilter)
            .Select(AssetDatabase.GUIDToAssetPath)
            .Where(a => !IsForbiddenExtension(a))
            .ToArray();
    }
        
    private static bool IsForbiddenExtension(string subject)
    {
        var lower = subject.ToLower();
        foreach (var extension in ForbiddenExtensions)
            if (lower.EndsWith(extension))
                return true;

        return false;
    }

    [Serializable, HideLabel, InlineProperty]
    public struct ReadonlyField
    {
        [HideLabel, ReadOnly, UsedImplicitly] public Object Value;
    }
}