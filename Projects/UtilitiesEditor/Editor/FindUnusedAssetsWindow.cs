﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UtilitiesEditor;

public class FindUnusedAssetsWindow : OdinEditorWindow
{
    [ReadOnly, ShowInInspector, ProgressBar(0, 100), PropertyOrder(0)]
    private float Loading => (totalCounter - (textsCounter + checksCounter) + 2) * 100f / totalCounter;

    [ShowInInspector, ListDrawerSettings(IsReadOnly = true), PropertyOrder(1)] private List<Object> unusedAssets;
    private int textsCounter;
    private int checksCounter;
    private int totalCounter;

    private string[] assetsToCheck;
    private ConcurrentBag<string> loadedTexts;
    private ConcurrentBag<string> unusedGuids;
        
    public void Init(string[] assets)
    {
        loadedTexts = new ConcurrentBag<string>();
        unusedAssets = new List<Object>();
        unusedGuids = new ConcurrentBag<string>();
            
        var paths = FindUsagesUtilities.CollectObjectPaths();
        textsCounter = paths.Length;
        checksCounter = assets.Length;
        totalCounter = textsCounter + checksCounter;
        assetsToCheck = assets;

        Task.Run(() => Parallel.ForEach(paths, a =>
        {
            loadedTexts.Add(File.ReadAllText(a));
            textsCounter--;
        }));
    }

    private void Update()
    {
        if (textsCounter == 0)
        {
            textsCounter = -1;
            ProcessLoadedTexts();
        }
        else if (checksCounter == 0)
        {
            checksCounter = -1;
            ProcessUnusedGuids();
        }
    }

    private void ProcessUnusedGuids()
        => unusedAssets = unusedGuids.Distinct().Select(a => AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(a))).ToList();

    private void ProcessLoadedTexts()
    {
        Task.Run(() => Parallel.ForEach(assetsToCheck, a =>
        {
            var isUsed = false;
            foreach (var loadedText in loadedTexts)
            {
                if (!loadedText.Contains(a)) continue;

                isUsed = true;
                break;
            }

            if (!isUsed)
                unusedGuids.Add(a);
            checksCounter--;
        }));
    }

    private void OnInspectorUpdate()
        => Repaint();
        
    [MenuItem("Tools/Find Unused Assets", priority = 0)]
    public static void ShowWindow()
    {
        GetWindow<FindUnusedAssetsWindow>().Init(FindUnusedAssetsUtilities.CollectGuids());
    }
}

internal static class FindUnusedAssetsUtilities
{
    private static readonly string[] IgnoredNamespaces = {"UnityEditor", "TMP", "UnityEngine", "DG", "TreeEditor", "Sirenix"};
    private static readonly string[] IgnoredTypes = {"Amplify", "ProceduralTexture"};
        
    public static string[] CollectGuids()
    {
        return AssetDatabase.FindAssets("t:ScriptableObject")
            .Select(AssetDatabase.GUIDToAssetPath)
            .SelectMany(AssetDatabase.LoadAllAssetsAtPath)
            .OfType<ScriptableObject>()
            .Where(a => !a.GetType().Namespace.IgnoredNamespacesContains())
            .Where(a => !a.GetType().Name.IgnoredTypesContains())
            .Select(AssetDatabase.GetAssetPath)
            .Select(AssetDatabase.AssetPathToGUID)
            .ToArray();
    }
        
    public static bool IgnoredNamespacesContains(this string target)
    {
        if (target == null) return false;

        foreach (var ns in IgnoredNamespaces)
            if (target.Contains(ns))
                return true;

        return false;
    }
        
    public static bool IgnoredTypesContains(this string target)
    {
        if (target == null) return false;

        foreach (var ns in IgnoredTypes)
            if (target.Contains(ns))
                return true;

        return false;
    }
}