﻿using System;
using System.Collections.Generic;

namespace Identifiers.Counters;

[Serializable]
public class Counters : ICounters
{
    private readonly Dictionary<int, ICounter> counters;

    [field: NonSerialized]
    private Dictionary<int, Action<CounterChangedInfo>> counterChanged = new();

    public int Count => counters.Count;

    public float this[int id] => counters[id].Value;

    public Counters()
    {
        counters = new Dictionary<int, ICounter>();
    }

    public Counters(CounterSerialized[] source)
    {
        counters = new Dictionary<int, ICounter>();
        foreach (var counterInfo in source)
        {
            var counter = new Counter(counterInfo.Id, counterInfo.Value);
            AssignInternal(counter);
        }
    }

    public void Subscribe(int id, Action<CounterChangedInfo> action)
    {
        if (!counterChanged.TryGetValue(id, out var existing)) 
            counterChanged[id] = action;
        else
            counterChanged[id] = existing + action;
    }

    public void Unsubscribe(int id, Action<CounterChangedInfo> action)
    {
        if (!counterChanged.TryGetValue(id, out var existing))
            throw new InvalidOperationException($"Counters does not contain subscription {id}");

        var newAction = existing - action;
        if (newAction != null)
            counterChanged[id] = newAction;
        else
            counterChanged.Remove(id);
    }

    private void AssignInternal(ICounter counter)
    {
        counters[counter.Id] = counter;
        counter.OnChangeIncremental += (a, b) => CounterChangedInternal(counter.Id, a, b);
    }

    private void CounterChangedInternal(int id, float oldValue, float newValue)
    {
        if (!counterChanged.TryGetValue(id, out var action))
            return;

        action?.Invoke(new CounterChangedInfo(id, oldValue, newValue));
    }

    public bool TryGet(int id, out ICounter counter)
        => counters.TryGetValue(id, out counter);

    public void Add(ICounter counter)
    {
        if (counters.ContainsKey(counter.Id))
            throw new InvalidOperationException($"Counters already contains key {counter.Id}");

        AssignInternal(counter);
    }

    public void Remove(int id)
    {
        if (!counters.TryGetValue(id, out var counter))
            throw new InvalidOperationException($"Counters does not contain key {id}");
            
        counter.Dispose();
        counters.Remove(id);
    }

    public void Dispose()
    {
        counterChanged.Clear();
        foreach (var kvp in counters) kvp.Value.Dispose();
        counters.Clear();
    }

    public Dictionary<int, ICounter>.Enumerator GetEnumerator()
        => counters.GetEnumerator();

    public bool Equals(ICounters other)
    {
        if (other == null) throw new NullReferenceException("[Counters.Equals]: other is null");

        if (Count != other.Count) return false;
            
        foreach (var counter in counters)
            if (!other.TryGet(counter.Key, out _))
                return false;

        return true;
    }

    public override string ToString()
        => $"Counters({string.Join("|", counters.Values)})";
}