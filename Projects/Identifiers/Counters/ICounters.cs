using System;

namespace Identifiers.Counters;

public interface ICounters : IDisposable, IEquatable<ICounters>
{
    int Count { get; }
    float this[int index] { get; }
    void Subscribe(int id, Action<CounterChangedInfo> action);
    void Unsubscribe(int id, Action<CounterChangedInfo> action);
    bool TryGet(int id, out ICounter counter);
    void Add(ICounter counter);
    void Remove(int id);
}