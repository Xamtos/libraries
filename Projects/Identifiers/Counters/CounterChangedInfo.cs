namespace Identifiers.Counters;

public readonly struct CounterChangedInfo
{
    public CounterChangedInfo(int id, float oldValue, float newValue)
    {
        Id = id;
        NewValue = newValue;
        OldValue = oldValue;
        Difference = NewValue - OldValue;
    }

    public int Id { get; }
    public float OldValue { get; }
    public float NewValue { get; }
    public float Difference { get; }
}