namespace Identifiers.Counters;

public interface IHaveCounters
{
    ICounters Counters { get; }
}