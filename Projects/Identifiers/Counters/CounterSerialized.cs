using System;

namespace Identifiers.Counters;

[Serializable]
public class CounterSerialized
{
    public int Id;
    public float Value;
}