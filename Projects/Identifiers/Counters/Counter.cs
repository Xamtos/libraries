﻿using System;
using Utilities;
using Utilities.Extensions;

namespace Identifiers.Counters;

[Serializable]
public class Counter : CallbackValue<float>, ICounter
{
    public Counter(int id, float value) : base(value)
    {
        Id = id;
    }

    public int Id { get; }
    public bool Equals(ICounter other)
    {
        if (other == null) throw new NullReferenceException("[Counter.Equals]: other is null");
            
        return other.Id == Id && other.Value.EqualsApprox(Value);
    }

    public override string ToString()
        => $"Counter({Id}|{IdGenerator.IdToName(Id)}|{Value})";
}

public interface ICounter : IIdentified, ICallbackValue<float>, IEquatable<ICounter>
{
}