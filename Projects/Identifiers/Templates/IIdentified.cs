﻿using System.Linq;

namespace Identifiers;

public interface IIdentified
{
    int Id { get; }
}

public static class IdentifiedExtensions
{
    public static int[] GetIds<T>(this T[] collection) where T : IIdentified
        => collection.Select(a => a.Id).ToArray();
}