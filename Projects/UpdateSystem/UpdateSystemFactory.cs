﻿using UpdateSystem.Modules;

namespace UpdateSystem;

public class UpdateSystemFactory
{
    public static IGlobalUpdateSystem Create()
        => new GlobalUpdateSystem();

    public static IGlobalUpdateSystem CreateEmpty()
        => new EmptyUpdateSystem();
}