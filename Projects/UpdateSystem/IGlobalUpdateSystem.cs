﻿using UpdateSystem.AwaitableOperations;

namespace UpdateSystem;

public interface IGlobalUpdateSystem : IPausable, IUpdateSystem, IUpdateSystemFixed, IUpdateSystemTimeStep, IDelayedActionSystem, IUpdateSystemUnscaled, IUpdateSystemConditional
{
}
    
public interface IUpdateSystem
{
    void Subscribe(IUpdatable updatable);
    void Unsubscribe(IUpdatable updatable);
}

public interface IUpdateSystemUnscaled
{
    void Subscribe(IUpdatableUnscaled updatable);
    void Unsubscribe(IUpdatableUnscaled updatable);
}
    
public interface IUpdateSystemFixed
{
    void Subscribe(IUpdatableFixed updatable);
    void Unsubscribe(IUpdatableFixed updatable);
}
    
public interface IUpdateSystemTimeStep
{
    void Subscribe(IUpdatableTimeStep updatable);
    void Unsubscribe(IUpdatableTimeStep updatable);
}

public interface IUpdateSystemConditional
{
    void Subscribe(IUpdatableConditional updatable);
    void Unsubscribe(IUpdatableConditional updatable);
}

public interface IDelayedActionSystem
{
    IAwaitableOperation Schedule(float delaySeconds);
}