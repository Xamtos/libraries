﻿using UnityEngine;
using UpdateSystem.AwaitableOperations;

namespace UpdateSystem.Modules;

internal class GlobalUpdateSystem : IGlobalUpdateSystem
{
    private readonly ModuleStandard moduleStandard;
    private readonly ModuleFixed moduleFixed;
    private readonly ModuleDelayed moduleDelayed;
    private readonly ModuleTimeStep moduleTimeStep;
    private readonly ModuleConditional moduleConditional;
    private readonly ModuleUnscaled moduleUnscaled;

    private bool isPaused;
    public bool IsPaused
    {
        get => isPaused;
        set
        {
            isPaused = value;
            moduleStandard.IsPaused = value;
            moduleFixed.IsPaused = value;
            moduleTimeStep.IsPaused = value;
            moduleConditional.IsPaused = value;
            moduleDelayed.IsPaused = value;
        }
    }

    public GlobalUpdateSystem()
    {
        var gameObject = new GameObject("UpdateSystem");
        moduleStandard = gameObject.AddComponent<ModuleStandard>();
        moduleFixed = gameObject.AddComponent<ModuleFixed>();
        moduleDelayed = gameObject.AddComponent<ModuleDelayed>();
        moduleTimeStep = gameObject.AddComponent<ModuleTimeStep>();
        moduleUnscaled = gameObject.AddComponent<ModuleUnscaled>();
        moduleConditional = gameObject.AddComponent<ModuleConditional>();
        Object.DontDestroyOnLoad(gameObject);
    }

    public void Subscribe(IUpdatable updatable)
        => moduleStandard.Subscribe(updatable);

    public void Unsubscribe(IUpdatable updatable)
        => moduleStandard.Unsubscribe(updatable);

    public void Subscribe(IUpdatableFixed updatable)
        => moduleFixed.Subscribe(updatable);

    public void Unsubscribe(IUpdatableFixed updatable)
        => moduleFixed.Unsubscribe(updatable);

    public void Subscribe(IUpdatableTimeStep updatable)
        => moduleTimeStep.Subscribe(updatable);

    public void Unsubscribe(IUpdatableTimeStep updatable)
        => moduleTimeStep.Unsubscribe(updatable);

    public IAwaitableOperation Schedule(float delaySeconds)
        => moduleDelayed.Schedule(delaySeconds);

    public void Subscribe(IUpdatableUnscaled updatable)
        => moduleUnscaled.Subscribe(updatable);

    public void Unsubscribe(IUpdatableUnscaled updatable)
        => moduleUnscaled.Unsubscribe(updatable);

    public void Subscribe(IUpdatableConditional updatable)
        => moduleConditional.Subscribe(updatable);

    public void Unsubscribe(IUpdatableConditional updatable)
        => moduleConditional.Subscribe(updatable);
}