using System;
using System.Collections.Generic;
using UnityEngine;

namespace UpdateSystem.Modules;

internal class ModuleFixed : MonoBehaviour, IUpdateSystemFixed, IPausable
{
    private readonly List<IUpdatableFixed> updatablesFixed = new List<IUpdatableFixed>();
    private readonly List<IUpdatableFixed> addQueue = new List<IUpdatableFixed>();
    private readonly List<IUpdatableFixed> removalQueue = new List<IUpdatableFixed>();

    public bool IsPaused { get; set; }

    public void Subscribe(IUpdatableFixed updatable)
        => addQueue.Add(updatable);

    public void Unsubscribe(IUpdatableFixed updatable)
        => removalQueue.Add(updatable);

    private void FixedUpdate()
    {
        if (IsPaused)
            return;
            
        foreach (var updatable in addQueue)
        {
            if (updatablesFixed.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleFixed)}: {nameof(IUpdatableFixed)} is already added.");
            updatablesFixed.Add(updatable);
        }

        foreach (var updatable in removalQueue)
        {
            if (!updatablesFixed.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleFixed)}: {nameof(IUpdatableFixed)} is already removed.");
            updatablesFixed.Remove(updatable);
        }
            
        addQueue.Clear();
        removalQueue.Clear();
        foreach (var updatable in updatablesFixed) updatable.FixedUpdate();
    }
}