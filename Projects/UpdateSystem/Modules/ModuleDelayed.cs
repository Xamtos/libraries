using System.Collections.Generic;
using UnityEngine;
using UpdateSystem.AwaitableOperations;

namespace UpdateSystem.Modules;

internal class ModuleDelayed : MonoBehaviour, IDelayedActionSystem, IPausable
{
    private readonly List<(float fireTime, AwaitableOperation operation)> delayActions = new List<(float fireTime, AwaitableOperation operation)>();
    public bool IsPaused { get; set; }

    private void Update()
    {
        if (IsPaused) return;
            
        float time = Time.time;
        for (int i = 0; i < delayActions.Count; i++)
        {
            var delayAction = delayActions[i];
            if (time < delayAction.fireTime) continue;

            delayActions.RemoveAt(i--);
            delayAction.operation.CallComplete();
        }
    }

    public IAwaitableOperation Schedule(float delaySeconds)
    {
        if (delaySeconds <= 0) return AwaitableOperationFactory.CompletedOperation;

        var asyncOperation = AwaitableOperationFactory.Pooled;
        var fireTime = Time.time + delaySeconds;
        delayActions.Add((fireTime, asyncOperation));
        return asyncOperation;
    }
}