using System;
using System.Collections.Generic;
using UnityEngine;

namespace UpdateSystem.Modules;

internal class ModuleUnscaled : MonoBehaviour, IUpdateSystemUnscaled
{
    private readonly List<IUpdatableUnscaled> updatablesUnscaled = new List<IUpdatableUnscaled>();
    private readonly List<IUpdatableUnscaled> addQueue = new List<IUpdatableUnscaled>();
    private readonly List<IUpdatableUnscaled> removalQueue = new List<IUpdatableUnscaled>();

    public void Subscribe(IUpdatableUnscaled updatable)
        => addQueue.Add(updatable);

    public void Unsubscribe(IUpdatableUnscaled updatable)
        => removalQueue.Add(updatable);

    private void Update()
    {
        foreach (var updatable in addQueue)
        {
            if (updatablesUnscaled.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleUnscaled)}: {nameof(IUpdatableUnscaled)} is already added.");
            updatablesUnscaled.Add(updatable);
        }

        foreach (var updatable in removalQueue)
        {
            if (!updatablesUnscaled.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleUnscaled)}: {nameof(IUpdatableUnscaled)} is already removed.");
            updatablesUnscaled.Remove(updatable);
        }

        addQueue.Clear();
        removalQueue.Clear();
        foreach (var updatableUnscaled in updatablesUnscaled) updatableUnscaled.UpdateUnscaled();
    }
}