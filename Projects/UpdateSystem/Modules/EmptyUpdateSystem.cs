﻿using UpdateSystem.AwaitableOperations;

namespace UpdateSystem.Modules;

internal class EmptyUpdateSystem : IGlobalUpdateSystem
{
    public bool IsPaused { get; set; } = false;
        
    public void Subscribe(IUpdatable updatable)
    { }

    public void Unsubscribe(IUpdatable updatable)
    { }

    public void Subscribe(IUpdatableFixed updatable)
    { }

    public void Unsubscribe(IUpdatableFixed updatable)
    { }

    public void Subscribe(IUpdatableTimeStep updatable)
    { }

    public void Unsubscribe(IUpdatableTimeStep updatable)
    { }

    public IAwaitableOperation Schedule(float delaySeconds)
        => default;

    public void Subscribe(IUpdatableUnscaled updatable)
    { }

    public void Unsubscribe(IUpdatableUnscaled updatable)
    { }

    public void Subscribe(IUpdatableConditional updatable)
    { }
        
    public void Unsubscribe(IUpdatableConditional updatable)
    { }
}