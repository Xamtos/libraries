using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UpdateSystem.Modules;

internal class ModuleTimeStep : MonoBehaviour, IUpdateSystemTimeStep, IPausable
{
    private readonly IDictionary<IUpdatableTimeStep, Coroutine> updatablesTimeStep = new Dictionary<IUpdatableTimeStep, Coroutine>();

    public bool IsPaused { get; set; }

    public void Subscribe(IUpdatableTimeStep updatable)
    {
        if (updatablesTimeStep.ContainsKey(updatable)) return;

        var coroutine = StartCoroutine(UpdateTimeStep(updatable));
        updatablesTimeStep.Add(updatable, coroutine);
    }

    public void Unsubscribe(IUpdatableTimeStep updatable)
    {
        if (updatablesTimeStep.TryGetValue(updatable, out var coroutine)) StopCoroutine(coroutine);
    }

    private IEnumerator UpdateTimeStep(IUpdatableTimeStep updatable)
    {
        while (true)
        {
            if (!IsPaused) updatable.Update();
            yield return updatable.TimeStep;
        }
    }
}