using System;
using System.Collections.Generic;
using UnityEngine;

namespace UpdateSystem.Modules;

internal class ModuleStandard : MonoBehaviour, IUpdateSystem, IPausable
{
    private readonly List<IUpdatable> updatables = new List<IUpdatable>();
    private readonly List<IUpdatable> addQueue = new List<IUpdatable>();
    private readonly List<IUpdatable> removalQueue = new List<IUpdatable>();

    public bool IsPaused { get; set; }

    public void Subscribe(IUpdatable updatable)
        => addQueue.Add(updatable);

    public void Unsubscribe(IUpdatable updatable)
        => removalQueue.Add(updatable);

    private void Update()
    {
        if (IsPaused) return;

        foreach (var updatable in addQueue)
        {
            if (updatables.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleStandard)}: {nameof(IUpdatable)} is already added.");
            updatables.Add(updatable);
        }

        foreach (var updatable in removalQueue)
        {
            if (!updatables.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleStandard)}: {nameof(IUpdatable)} is already removed.");
            updatables.Remove(updatable);
        }

        addQueue.Clear();
        removalQueue.Clear();
        foreach (var updatable in updatables) updatable.Update();
    }
}