using System;
using System.Collections.Generic;
using UnityEngine;

namespace UpdateSystem.Modules;

internal class ModuleConditional : MonoBehaviour, IUpdateSystemConditional, IPausable
{
    private readonly List<IUpdatableConditional> updatables = new List<IUpdatableConditional>();
    private readonly List<IUpdatableConditional> addQueue = new List<IUpdatableConditional>();
    private readonly List<IUpdatableConditional> removalQueue = new List<IUpdatableConditional>();

    public bool IsPaused { get; set; }

    public void Subscribe(IUpdatableConditional updatable)
        => addQueue.Add(updatable);

    public void Unsubscribe(IUpdatableConditional updatable)
        => removalQueue.Add(updatable);

    private void Update()
    {
        if (IsPaused) return;

        foreach (var updatable in addQueue)
        {
            if (updatables.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleConditional)}: {nameof(IUpdatableConditional)} is already added.");
            updatables.Add(updatable);
        }

        foreach (var updatable in removalQueue)
        {
            if (!updatables.Contains(updatable)) throw new InvalidOperationException($"{nameof(ModuleConditional)}: {nameof(IUpdatableConditional)} is already removed.");
            updatables.Remove(updatable);
        }

        addQueue.Clear();
        removalQueue.Clear();
        foreach (var updatable in updatables)
        {
            if (updatable.Condition)
                updatable.Update();
        }
    }
}