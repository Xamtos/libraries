﻿using System;

namespace UpdateSystem.AwaitableOperations;

public class AwaitableOperation : IAwaitableOperation
{
    public Action Completed { get; set; }
    public bool IsCompleted { get; set; }
    
    public void CallComplete()
    {
        IsCompleted = true;
        Completed?.Invoke();
        AwaitableOperationFactory.Pool.Enqueue(this);
    }
}
    
public interface IAwaitableOperation
{
    Action Completed { get; set; }
    bool IsCompleted { get; }
}