using System;
using System.Runtime.CompilerServices;

namespace UpdateSystem.AwaitableOperations;

public readonly struct AwaitableOperationAwaiter : INotifyCompletion
{
    private readonly IAwaitableOperation asyncOperation;

    public AwaitableOperationAwaiter(IAwaitableOperation asyncOperation)
    {
        this.asyncOperation = asyncOperation;
    }
        
    public void OnCompleted(Action onCompleted)
    {
        if (!asyncOperation.IsCompleted)
            asyncOperation.Completed += onCompleted;
        else
            onCompleted?.Invoke();
    }

    public void GetResult(){}
 
    public bool IsCompleted => asyncOperation.IsCompleted;

}
    
public static class AwaiterExtensions
{
    public static AwaitableOperationAwaiter GetAwaiter(this IAwaitableOperation asyncOperation)
        => new AwaitableOperationAwaiter(asyncOperation);
}