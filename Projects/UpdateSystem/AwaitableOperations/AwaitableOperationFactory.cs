using System.Collections.Generic;

namespace UpdateSystem.AwaitableOperations;

public static class AwaitableOperationFactory
{
    internal static readonly Queue<AwaitableOperation> Pool = new Queue<AwaitableOperation>();
    public static AwaitableOperation CompletedOperation { get; } = new AwaitableOperation {IsCompleted = true};
    public static AwaitableOperation Pooled 
    {
        get
        {
            if (Pool.Count > 0)
            {
                var operation = Pool.Dequeue();
                operation.Completed = null;
                operation.IsCompleted = false;
                return operation;
            }

            return new AwaitableOperation();
        }
    }
}