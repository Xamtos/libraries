﻿namespace UpdateSystem;

public interface IPausable
{
    bool IsPaused { get; set; }
}