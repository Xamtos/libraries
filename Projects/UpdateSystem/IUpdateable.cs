﻿using UnityEngine;

namespace UpdateSystem;

public interface IUpdatable
{
    void Update();
}
    
public interface IUpdatableUnscaled
{
    void UpdateUnscaled();
}
    
public interface IUpdatableFixed
{
    void FixedUpdate();
}

public interface IUpdatableTimeStep : IUpdatable
{
    YieldInstruction TimeStep { get; }
}
    
public interface IUpdatableConditional
{
    void Update();
    bool Condition { get; }
}