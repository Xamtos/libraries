﻿using System;
using JetBrains.Annotations;

namespace Utilities.Monads;

[PublicAPI]
public abstract class Maybe<T> : IEquatable<Maybe<T>>
{
    private Maybe()
    {
    }

    public static explicit operator Maybe<T>(T value)
        => value == null ? None : Some(value);

    public static Maybe<T> Some(T value)
    {
        if (value == null) throw new NullReferenceException(nameof(value));

        return new MaybeSome(value);
    }

    public static Maybe<T> None { get; } = new MaybeNone();

    public bool IsNone => this is MaybeNone;

    public bool IsSome => this is MaybeSome;

    public abstract TResult Match<TResult>(Func<T, TResult> someFunc, Func<TResult> noneFunc);

    public abstract void Match(Action<T>? someAction, Action? noneAction);

    public Maybe<TResult> Map<TResult>(Func<T, TResult> map) where TResult : notnull
        => Match(a => Maybe<TResult>.Some(map(a)), () => Maybe<TResult>.None);

    public Maybe<TResult> Bind<TResult>(Func<T, Maybe<TResult>> map) where TResult : notnull
        => Match(a => map(a).Match(Maybe<TResult>.Some, () => Maybe<TResult>.None), () => Maybe<TResult>.None);
        
    bool IEquatable<Maybe<T>>.Equals(Maybe<T> other)
        => Equals(other);
        
    public sealed class MaybeSome : Maybe<T>
    {
        private T Value { get; }
            
        public MaybeSome(T value)
            => Value = value;

        public override TResult Match<TResult>(Func<T, TResult> someFunc, Func<TResult> noneFunc)
            => someFunc(Value);

        public override void Match(Action<T>? someAction, Action? noneAction)
            => someAction?.Invoke(Value);

        public override string ToString()
            => $"Some({Value})";
    }

    public sealed class MaybeNone : Maybe<T>
    {
        public override TResult Match<TResult>(Func<T, TResult> someFunc, Func<TResult> noneFunc)
            => noneFunc();

        public override void Match(Action<T>? someAction, Action? noneAction)
            => noneAction?.Invoke();

        public override string ToString()
            => "None";
    }
}