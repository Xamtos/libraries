﻿using System;
using JetBrains.Annotations;

namespace Utilities.Monads;

[PublicAPI]
public sealed class Either<TLeft, TRight> where TLeft : notnull where TRight : notnull
{
    private readonly TLeft left;
    private readonly TRight right;
    private readonly bool isLeft;

    public Either(TLeft left)
    {
        this.left = left;
        right = default!;
        isLeft = true;
    }

    public Either(TRight right)
    {
        this.right = right;
        left = default!;
        isLeft = false;
    }

    public T Match<T>(Func<TLeft, T> leftFunc, Func<TRight, T> rightFunc)
        => isLeft ? leftFunc(left) : rightFunc(right);

    public TLeft LeftOrDefault() => Match(l => l, r => default!);

    public TRight RightOrDefault() => Match(l => default!, r => r);

    public static implicit operator Either<TLeft, TRight>(TLeft left) => new Either<TLeft, TRight>(left);

    public static implicit operator Either<TLeft, TRight>(TRight right) => new Either<TLeft, TRight>(right);
}