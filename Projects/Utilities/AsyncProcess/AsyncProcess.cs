﻿using System;
using JetBrains.Annotations;

namespace Utilities.AsyncProcess;

[UsedImplicitly]
public abstract class AsyncProcess : IAsyncProcess
{
    private event Action CompletedBacking = null!;

    [UsedImplicitly]
    protected void CompleteAndClear()
    {
        IsComplete = true;
        CompletedBacking?.Invoke();
        CompletedBacking = null!;
    }

    [UsedImplicitly]
    public bool IsComplete { get; protected set; }
        
    public event Action Completed
    {
        add
        {
            if (IsComplete)
                value();
            else
                CompletedBacking += value;
        }
        remove => CompletedBacking -= value;
    }
}

public interface IAsyncProcess
{
    [UsedImplicitly] bool IsComplete { get; }
    [UsedImplicitly] event Action Completed;
}