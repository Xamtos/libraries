﻿using System;
using JetBrains.Annotations;

namespace Utilities.AsyncProcess;

[UsedImplicitly]
public abstract class AsyncProcess<T> : IAsyncProcess<T>
{
    private event Action<T> CompletedBacking = null!;

    [UsedImplicitly]
    protected void CompleteAndClear(T value)
    {
        Result = value;
        IsComplete = true;
        CompletedBacking?.Invoke(value);
        CompletedBacking = null!;
    }

    [UsedImplicitly]
    public bool IsComplete { get; protected set; }
    public event Action<T> Completed
    {
        add
        {
            if (IsComplete)
                value(Result!);
            else
                CompletedBacking += value;
        }
        remove => CompletedBacking -= value;
    }

    public T? Result { get; private set; }
}

public interface IAsyncProcess<out T>
{
    [UsedImplicitly] bool IsComplete { get; }
    [UsedImplicitly] event Action<T> Completed;
    [UsedImplicitly] T? Result { get; }
}