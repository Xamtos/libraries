﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Extensions;

[ExcludeFromCodeCoverage, PublicAPI]
public static class UnityExtensions
{
    private static RaycastHit[] hitsBuffer = new RaycastHit[1];
        
    /// <summary> Fire raycast along the ray with specified distance and layer mask. </summary>
    /// <returns>Distance to nearest collider or infinity.</returns>
    public static float MeasureRaycastDistance(this Ray ray, float max = Mathf.Infinity, int layerMask = -1, QueryTriggerInteraction triggerInteraction = QueryTriggerInteraction.Ignore)
    {
        var count = Physics.RaycastNonAlloc(ray, hitsBuffer, max, layerMask, triggerInteraction);
        return count > 0 ? hitsBuffer[0].distance : Mathf.Infinity;
    }

    /// <summary> Fire raycast along the ray with specified distance and layer mask. </summary>
    /// <returns>Point of RaycastHit or default.</returns>
    public static Vector3 GetRaycastHitPoint(this Ray ray, float max = Mathf.Infinity, int layerMask = -1, QueryTriggerInteraction triggerInteraction = QueryTriggerInteraction.Ignore)
    {
        var count = Physics.RaycastNonAlloc(ray, hitsBuffer, max, layerMask, triggerInteraction);
        return count > 0 ? hitsBuffer[0].point : default;
    }

    /// <summary>
    /// Determines whether there is collider along the ray within specified distance.
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    public static bool RaycastInRange(this Ray ray, float range, int layerMask = -1, QueryTriggerInteraction triggerInteraction = QueryTriggerInteraction.Ignore)
    {
        if (range < 0) throw new ArgumentException($"{nameof(RaycastInRange)}: range cannot be less than zero.");

        float result = ray.MeasureRaycastDistance(range, layerMask, triggerInteraction);
        return result < range;
    }

    public static void SetHeight(this Transform transform, float value)
    {
        var position = transform.position;
        position.y = value;
        transform.position = position;
    }

    public static void SetLayer(this Transform transform, int layer, bool includeChildren = false)
    {
        if (!includeChildren)
        {
            transform.gameObject.layer = layer;
            return;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            child.SetLayer(layer, true);
        }
    }
}