﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Extensions;

[PublicAPI]
public static class StructsExtensions
{
    public static Color SetAlpha(this Color color, float alpha)
    {
        color.a = alpha;
        return color;
    }
        
    public static int CombineHashCode(this int a, int b)
    {
        unchecked
        {
            int hash = 17;
            hash = hash * 31 + a;
            hash = hash * 31 + b;
            return hash;
        }
    }

    public static int CombineHashCode(this object a, object b)
    {
        unchecked
        {
            int hash = 17;
            hash = hash * 31 + a.GetHashCode();
            hash = hash * 31 + b.GetHashCode();
            return hash;
        }
    }

    public static long ToInt(this DateTime time)
    {
        DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        TimeSpan tsInterval = time.Subtract(dt1970);
        return Convert.ToInt32(tsInterval.TotalSeconds);
    }

    public static DateTime ToDateTime(this long time)
    {
        DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        TimeSpan tsInterval = TimeSpan.FromSeconds(time);
        return dt1970 + tsInterval;
    }
}