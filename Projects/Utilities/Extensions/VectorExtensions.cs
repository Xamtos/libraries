﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Utilities.Extensions;

[PublicAPI]
public static class VectorExtensions
{
    private const int RandomPositionsCounterAmount = 100;
    
    public static bool TryGetRandomPositions(this Vector3 position, int count, float radius, ref List<Vector3> positions, float positionTolerance = 0.2f)
    {
        var checker = RandomPositionsCounterAmount;
        positions.Clear();
        for (int i = 0; i < count; i++)
        {
            if (checker == 0)
            {
                positions.Clear();
                return false;
            }
                
            Vector3 newPosition = position + Random.insideUnitCircle.ToVector3() * radius;
            if (NewPositionTooCloseToExisting(newPosition, ref positions, positionTolerance))
            {
                i--;
                checker--;
                continue;
            }
            
            positions.Add(newPosition);
            checker = RandomPositionsCounterAmount;
        }

        return true;
    }

    private static bool NewPositionTooCloseToExisting(Vector3 newPosition, ref List<Vector3> positions, float tolerance = 0.2f)
    {
        foreach (Vector3 existing in positions)
            if (newPosition.DistanceSqr(existing) <= Mathf.Pow(tolerance, 2))
                return true;

        return false;
    }

    public static Vector3 SampleNavMeshPosition(this Vector3 position, int navmeshArea)
    {
        // Find closest NavMesh point: cheap version first and heavy if the first one failed.
        if (NavMesh.SamplePosition(position, out NavMeshHit hit, .5f, navmeshArea)) return hit.position;
        if (NavMesh.SamplePosition(position, out hit, 4, navmeshArea)) return hit.position;

        throw new InvalidOperationException($"Couldn't sample NavMesh at position: {position}.");
    }

    public static float Avg(this Vector3 source)
        => (Math.Abs(source.x) + Math.Abs(source.y) + Math.Abs(source.z)) / 3;

    public static float Avg(this Vector2 source)
        => (Math.Abs(source.x) + Math.Abs(source.y)) / 2;

    public static float Avg(this Vector2Int source)
        => (Math.Abs(source.x) + Math.Abs(source.y)) / 2f;

    public static float DistanceSqr(this Vector3 a, Vector3 b)
    {
        float num1 = a.x - b.x;
        float num2 = a.y - b.y;
        float num3 = a.z - b.z;
        return num1 * num1 + num2 * num2 + num3 * num3;
    }

    /// <summary>
    /// Returns true if vector magnitude exceeds provided value.
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    public static bool GreaterThan(this Vector3 vector, float max)
    {
        if (max < 0) throw new ArgumentException($"{nameof(GreaterThan)}: magnitude cannot be less than zero.");

        return vector.sqrMagnitude > Mathf.Pow(max, 2);
    }

    /// <summary>
    /// Transforms Vector3 to Vector2 in which former z-axis becomes y-axis.
    /// </summary>
    public static Vector2 ToVector2(this Vector3 vector)
        => new Vector2(vector.x, vector.z);

    /// <summary>
    /// Transforms Vector2 to Vector3 in which former y-axis becomes z-axis.
    /// </summary>
    /// <param name="vector">Vector to transform.</param>
    /// <param name="height">New y-axis value of Vector3.</param>
    public static Vector3 ToVector3(this Vector2 vector, float height = 0)
        => new Vector3(vector.x, height, vector.y);

    public static bool IsZero(this Vector3 vector)
        => vector == default;
        
    public static bool IsZero(this Vector2 vector)
        => vector == default;
}