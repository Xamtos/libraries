﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Utilities.Extensions;

[PublicAPI]
public static class CollectionsExtensions
{
    public static bool TryGetOfType<TType, T>(this List<T>? source, out TType result)
    {
        result = default!;
        if (source == null) return false;

        foreach (var element in source)
        {
            if (!(element is TType t)) continue;

            result = t;
            return true;
        }

        return false;
    }

    public static bool IsNotNullRecursively<T>(this IEnumerable<T>? source) where T : class
    {
        if (source == null) return false;

        foreach (var a in source)
            if (a == null)
                return false;

        return true;
    }

    public static bool IsNotNullRecursively<T>(this List<T>? source) where T : class
    {
        if (source == null) return false;

        foreach (var a in source)
            if (a == null)
                return false;

        return true;
    }
        
    public static void RemoveDuplicates<T>(this IList<T> collection) where T : IEquatable<T>
    {
        for (int i = collection.Count - 1; i >= 0; i--)
        {
            var element = collection[i];
            for (int j = i - 1; j >= 0; j--)
            {
                if (!collection[j].Equals(element)) continue;

                collection.RemoveAt(i);
                break;
            }
        }
    }

    public static void AddToLookup<TKey, TValue>(this IDictionary<TKey, List<TValue>> dictionary, TKey key, TValue value)
    {
        if (dictionary.TryGetValue(key, out var collection))
            collection.Add(value);
        else
            dictionary[key] = new List<TValue> { value };
    }
        
    public static void RemoveFromLookup<TKey, TValue>(this IDictionary<TKey, List<TValue>> dictionary, TKey key, TValue value)
    {
        if (dictionary.TryGetValue(key, out var collection))
            collection.Remove(value);
        else
            throw new KeyNotFoundException();
    }

    public static void ClearRecursively<TKey, TValue>(this IDictionary<TKey, List<TValue>> dictionary)
    {
        foreach (var kvp in dictionary) kvp.Value.Clear();
        dictionary.Clear();
    }

    public static T SingleOfType<T>(this IEnumerable source)
    {
        T result = default!;
        int counter = 0;
        foreach (var obj in source)
        {
            if (obj is not T tResult) continue;

            result = tResult;
            counter++;
        }

        switch (counter)
        {
            case 0: throw new ArgumentOutOfRangeException();
            case 1 : return result;
            default: throw new InvalidOperationException();
        }
    }
}