﻿using System;
using System.Collections.Generic;

namespace Utilities.Extensions;

public static class RandomExtensions
{
    private static readonly Random rnd = new Random();

    /// <summary>
    /// Returns random element of collection. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    public static T RandomElement<T>(this IEnumerable<T> collection, Random? seed = null!)
    {
        seed ??= rnd;
        if (collection == null) 
            throw new ArgumentNullException($"{nameof(RandomElement)}: Collection is null.");
            
        T current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }
            
        return count != 0 ? current : throw new ArgumentException($"{nameof(RandomElement)}: Collection is empty.");
    }
        
    /// <summary>
    /// Returns random element of collection by predicate. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool TryGetRandomElement<T>(this IEnumerable<T> collection, out T current, Func<T, bool> predicate, Random? seed = null!)
    {
        seed ??= rnd;
        if (collection == null) 
            throw new ArgumentNullException($"{nameof(TryGetRandomElement)}: Collection is null.");
            
        current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            if (!predicate(element))
                continue;
                
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }

        return count != 0;
    }

    /// <summary>
    /// Returns random element of collection. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool TryGetRandomElement<T>(this IEnumerable<T> collection, out T current, Random? seed = null!)
    {
        seed ??= rnd;
        if (collection == null) 
            throw new ArgumentNullException($"{nameof(TryGetRandomElement)}: Collection is null.");
            
        current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }

        return count != 0;
    }
        
    /// <summary>
    /// Returns collection of random elements from source, with specified elements count. 
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static IEnumerable<T> RandomElements<T>(this IEnumerable<T> collection, int count, Random? seed = null!)
    {
        var result = new List<T>(count);
        collection.RandomElementsNonAlloc(ref result, count, seed);
        return result;
    }
        
    /// <summary>
    /// Returns collection of random elements from source, with specified elements count. 
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
    public static void RandomElementsNonAlloc<T>(this IEnumerable<T> collection, ref List<T> result, int count, Random? seed = null!)
    {
        seed ??= rnd;
        if (count < 0)
            throw new ArgumentException($"{nameof(RandomElements)}: Count must be non-negative number.");
            
        if (collection == null) 
            throw new ArgumentNullException($"{nameof(RandomElements)}: {nameof(collection)} is null");
        if (result == null) 
            throw new ArgumentNullException($"{nameof(RandomElements)}: {nameof(result)} is null");

        result.Clear();
        if (count == 0)
            return;

        result.AddRange(collection);
        if (result.Count < count)
            throw new ArgumentException($"{nameof(RandomElements)}: Count is greater than collection length.");

        // Remove random elements from collection.
        while (result.Count > count) result.RemoveAt(seed.Next(0, result.Count));
            
        // Rearrange elements order.
        for (int i = result.Count - 1; i > 0; i--)
        {
            int index = seed.Next(0, i);  
            (result[index], result[i]) = (result[i], result[index]);
        }
    }
}