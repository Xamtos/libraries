﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace Utilities.Extensions;

[PublicAPI]
public static class SharpExtensions
{
    public static bool IsNullOrEmpty(this string str)
        => string.IsNullOrEmpty(str);

    /// <summary>
    /// Checks if string contains letters and digits only.
    /// </summary>
    public static bool LettersAndDigitsOnly(this string check) 
        => !string.IsNullOrEmpty(check) && !Regex.Match(check, @"[^A-Za-z0-9]+").Success;

    /// <summary>
    /// Checks if argument is anonymous method info.
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool IsAnonymous(this MethodInfo methodInfo)
    {
        if (methodInfo == null) throw new ArgumentNullException(nameof(methodInfo));
            
        var invalidChars = new[] {'<', '>'};
        return methodInfo.Name.Any(invalidChars.Contains);
    }

    public static bool EqualsApprox(this float a, float b, float tolerance = .0001f)
        => Math.Abs(a - b) < tolerance;

    public static string ToTitleCase(this string input, bool forceFirstAsLower = false)
    {
        StringBuilder stringBuilder = new();
        if (forceFirstAsLower)
            stringBuilder.Append(char.ToLowerInvariant(input[0]));
        else
            stringBuilder.Append(char.ToUpperInvariant(input[0]));
        for (int index = 1; index < input.Length; ++index)
        {
            char ch = input[index];
                
            if (ch == '_' && index + 1 < input.Length)
            {
                char letter = input[index + 1];
                stringBuilder.Append(char.ToUpperInvariant(letter));
                ++index;
            }
            else
                stringBuilder.Append(ch);
        }
        return stringBuilder.ToString();
    }
}