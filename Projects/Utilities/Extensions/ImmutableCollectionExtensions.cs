﻿using System;
using JetBrains.Annotations;

namespace Utilities.Extensions;

[PublicAPI]
public static class ImmutableCollectionExtensions
{
    private static readonly Random rnd = new Random();

    /// <summary>
    /// Returns random element of collection. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    public static T RandomElement<T>(this ImmutableCollection<T> collection, Random? seed = null!)
    {
        seed ??= rnd;
        T current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }
            
        return count != 0 ? current : throw new ArgumentException($"{nameof(RandomElement)}: Collection is empty.");
    }
    
    /// <summary>
    /// Returns random element of collection by predicate. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool TryGetRandomElement<T>(this ImmutableCollection<T> collection, out T current, Func<T, bool> predicate, Random? seed = null!)
    {
        seed ??= rnd;
        current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            if (!predicate(element))
                continue;
                
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }

        return count != 0;
    }
    
    /// <summary>
    /// Returns random element of collection. 
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool TryGetRandomElement<T>(this ImmutableCollection<T> collection, out T current, Random? seed = null!)
    {
        seed ??= rnd;
        current = default!;
        var count = 0;
        foreach (T element in collection)
        {
            count++;
            if (seed.Next(0, count) == 0) current = element;
        }

        return count != 0;
    }
}