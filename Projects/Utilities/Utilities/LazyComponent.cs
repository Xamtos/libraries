﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Utilities;

public class LazyComponent<T> where T : Component
{
    private readonly Component source;
    private readonly bool includeChildren;
        
    private T cached;
    private bool isInited;

    public LazyComponent(Component source, bool includeChildren = false)
    {
        this.source = source;
        cached = null!;
        isInited = false;
        this.includeChildren = includeChildren;
    }

    public T Get()
    {
        if (isInited) return cached;

        isInited = true;
        cached = includeChildren ? source.GetComponentInChildren<T>(): source.GetComponent<T>();
        Assert.IsNotNull(cached);
        return cached;
    }
        
    public static implicit operator T(LazyComponent<T> lazy) => lazy.Get();
}
    
public class LazyComponents<T> where T : Component
{
    private readonly Component source;
        
    private T[] cached;
    private bool isInited;

    public LazyComponents(Component source)
    {
        this.source = source;
        cached = null!;
        isInited = false;
    }

    public T[] Get()
    {
        if (isInited) return cached;

        isInited = true;
        cached = source.GetComponentsInChildren<T>();
        Assert.IsNotNull(cached);
        return cached;
    }

    public T this[int index]
    {
        get
        {
            if (isInited) return cached[index];

            isInited = true;
            cached = source.GetComponentsInChildren<T>();
            Assert.IsNotNull(cached);
            return cached[index];
        }
    }
}