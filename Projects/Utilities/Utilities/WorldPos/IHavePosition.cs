﻿namespace Utilities;

public interface IHavePosition
{
    WorldPos WorldPos { get; }
}
    
public interface IHavePositionReactive
{
    ICallbackValueReadonly<WorldPos> WorldPos { get; }
}