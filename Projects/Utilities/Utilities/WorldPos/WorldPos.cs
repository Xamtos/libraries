﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using Utilities.Extensions;

namespace Utilities;

[PublicAPI]
public readonly struct WorldPos : IEquatable<WorldPos>
{
    public Vector3 Position { get; }
    public Quaternion Rotation { get; }
    public Vector3 Forward => Rotation * Vector3.forward;

    public WorldPos(Vector3 position, Quaternion rotation)
    {
        Position = position;
        Rotation = rotation;
    }

    public WorldPos(Vector2 position, Quaternion rotation) : this(position.ToVector3(), rotation)
    {
    }

    public bool Equals(WorldPos other)
        => Position.Equals(other.Position) && Rotation.Equals(other.Rotation);

    public override bool Equals(object obj)
        => obj is WorldPos other && Equals(other);

    public override int GetHashCode()
        => Position.GetHashCode().CombineHashCode(Rotation.GetHashCode());

    public override string ToString()
        => $"WorldPos({Position}|{Rotation.eulerAngles})";

    public static WorldPos operator +(WorldPos a, WorldPos b)
        => new WorldPos(a.Position + b.Position, b.Rotation);

    public static WorldPos operator -(WorldPos a, WorldPos b)
        => new WorldPos(a.Position - b.Position, b.Rotation);

    public static WorldPos Default { get; } = new WorldPos(Vector3.zero, Quaternion.identity);
}