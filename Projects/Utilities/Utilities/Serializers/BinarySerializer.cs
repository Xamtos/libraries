﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Utilities.Monads;

namespace Utilities.Serializers;

/// <inheritdoc />
[PublicAPI]
public class BinarySerializer : ISerializer
{
    private readonly string path;
        
    /// <summary>
    /// Serializer with specified absolute file path to serialize/deserialize.
    /// </summary>
    public BinarySerializer(string path)
    {
        this.path = path;
            
        string name = Path.GetDirectoryName(path) ?? throw new ArgumentException($"{nameof(BinarySerializer)}: incorrect path");
        if (!Directory.Exists(name))
            Directory.CreateDirectory(name);
    }

    /// <inheritdoc />
    public async Task<Maybe<string>> TrySaveAsync<T>(T objToSerialize) where T : notnull
        => await Task.Run(() => TrySave(objToSerialize));

    public Maybe<string> TrySave<T>(T objToSerialize) where T : notnull
    {
        try
        {
            using Stream stream = File.Open(path, FileMode.Create);
            var bin = new BinaryFormatter();
            bin.Serialize(stream, objToSerialize);
            return Maybe<string>.None;
        }
        catch (Exception ex)
        {
            return Maybe<string>.Some(ex.ToString());
        }
    }

    /// <inheritdoc />
    public Maybe<T> TryLoad<T>() where T : notnull
    {
        try
        {
            using Stream stream = File.Open(path, FileMode.Open);
            var bin = new BinaryFormatter();
            stream.Position = 0;
            var result = bin.Deserialize(stream);
            return Maybe<T>.Some((T)result);
        }
        catch
        {
            return Maybe<T>.None;
        }
    }

    /// <inheritdoc />
    public Either<T, string> TryLoadWithErrorMessage<T>() where T : notnull
    {
        try
        {
            using Stream stream = File.Open(path, FileMode.Open);
            var bin = new BinaryFormatter();
            return new Either<T, string>((T)bin.Deserialize(stream));
        }
        catch (Exception ex)
        {
            return new Either<T, string>(ex.ToString());
        }
    }
}