﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Utilities.Serializers;

[Serializable]
[DataContract]
internal struct Vector2Serializable
{
    [DataMember] private float x;
    [DataMember] private float y;
        
    public Vector2Serializable(Vector2 source)
    {
        x = source.x;
        y = source.y;
    }

    public Vector2 Convert => new Vector2(x, y);
}