﻿using System;
using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using Utilities.Monads;

namespace Utilities.Serializers;

/// <inheritdoc />
[PublicAPI]
public class JsonSerializer : ISerializer
{
    private readonly string path;
        
    /// <summary>
    /// Serializer with specified absolute file path to serialize/deserialize.
    /// </summary>
    public JsonSerializer(string path)
    {
        this.path = path;
            
        string name = Path.GetDirectoryName(path) ?? throw new ArgumentException($"{nameof(BinarySerializer)}: incorrect path");
        if (!Directory.Exists(name))
            Directory.CreateDirectory(name);
    }

    /// <inheritdoc />
    public async Task<Maybe<string>> TrySaveAsync<T>(T objToSerialize) where T : notnull
        => await Task.Run(() => TrySave(objToSerialize));

    public Maybe<string> TrySave<T>(T objToSerialize) where T : notnull
    {
        try
        {
            File.WriteAllText(path, JsonUtility.ToJson(objToSerialize));
            return Maybe<string>.None;
        }
        catch (Exception ex)
        {
            return Maybe<string>.Some(ex.ToString());
        }
    }

    /// <inheritdoc />
    public Maybe<T> TryLoad<T>() where T : notnull
    {
        try
        {
            var text = File.ReadAllText(path);
            return Maybe<T>.Some(JsonUtility.FromJson<T>(text));
        }
        catch
        {
            return Maybe<T>.None;
        }
    }

    /// <inheritdoc />
    public Either<T, string> TryLoadWithErrorMessage<T>() where T : notnull
    {
        try
        {
            var text = File.ReadAllText(path);
            return new Either<T, string>(JsonUtility.FromJson<T>(text));
        }
        catch (Exception ex)
        {
            return new Either<T, string>(ex.ToString());
        }
    }    }