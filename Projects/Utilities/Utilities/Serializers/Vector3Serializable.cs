﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Utilities.Serializers;

[Serializable]
[DataContract]
internal readonly struct Vector3Serializable
{
    [DataMember] public float X { get; }
    [DataMember] public float Y { get; }
    [DataMember] public float Z { get; }
        
    public Vector3Serializable(Vector3 source)
    {
        X = source.x;
        Y = source.y;
        Z = source.z;
    }

    public Vector3Serializable(Vector2 source)
    {
        X = source.x;
        Y = source.y;
        Z = 0;
    }
        
    public Vector3Serializable(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public Vector3 Convert => new Vector3(X, Y, Z);
}