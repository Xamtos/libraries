﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Utilities.Monads;

namespace Utilities.Serializers;

/// <summary>
/// Serialization and deserialization of generic object.
/// </summary>
[PublicAPI]
public interface ISerializer
{
    /// <summary> Serialize object. </summary>
    /// <param name="objToSerialize"> Object for serialization. Must be marked <c>[System.Serializable]</c>.</param>
    /// <returns> Optional error message.</returns>
    Task<Maybe<string>> TrySaveAsync<T>(T objToSerialize) where T : notnull;
    Maybe<string> TrySave<T>(T objToSerialize) where T : notnull;
        
    /// <summary> Deserialize object of type T. </summary>
    /// <returns> Deserialized value. </returns>
    Maybe<T> TryLoad<T>() where T : notnull;
        
    /// <summary> Deserialize object of type T from a file. </summary>
    /// <returns> Deserialized value or error message. </returns>
    Either<T, string> TryLoadWithErrorMessage<T>() where T : notnull;
}