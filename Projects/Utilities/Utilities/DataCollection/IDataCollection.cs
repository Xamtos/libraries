﻿using JetBrains.Annotations;
using UnityEngine;

namespace Utilities;

/// <summary>
/// Collection that support data saving and retrieving.
/// </summary>
[PublicAPI]
public interface IDataCollection
{
    void Add(int key, int data);
    void Add(int key, float data);
    void Add(int key, bool data);
    void Add(int key, Vector3 data);
    void Add(int key, Vector2 data);
    void Add<T>(int key, T data) where T : class;

    bool TryGet(int key, out int value);
    bool TryGet(int key, out float value);
    bool TryGet(int key, out bool value);
    bool TryGet(int key, out Vector3 value);
    bool TryGet(int key, out Vector2 value);
    bool TryGet<T>(int key, out T value) where T : class;

    int GetInt(int key);
    float GetFloat(int key);
    bool GetBool(int key);
    Vector3 GetVector3(int key);
    Vector2 GetVector2(int key);
    T Get<T>(int key) where T : class;

    void Remove(int key);
    void Clear();
    bool IsEmpty { get; }
}