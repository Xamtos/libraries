﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using UnityEngine;
using Utilities.Extensions;
using Utilities.Serializers;

namespace Utilities;

/// <summary>
/// Class that support data saving and retrieving.
/// </summary>
[Serializable]
[DataContract]
[PublicAPI]
public sealed class DataCollection : IDataCollection, IEquatable<IDataCollection>, IEquatable<DataCollection>
{
    [DataMember]
    private Dictionary<int, object> objects = new Dictionary<int, object>();
    [DataMember]
    private Dictionary<int, Vector3Serializable> vectors = new Dictionary<int, Vector3Serializable>();
    [DataMember]
    private Dictionary<int, double> values = new Dictionary<int, double>();

    public static IDataCollection Default { get; } = new NullDataCollection();

    public void Add(int key, int data)
        => values[key] = Convert.ToDouble(data);
    public void Add(int key, float data)
        => values[key] = Convert.ToDouble(data);
    public void Add(int key, bool data)
        => values[key] = Convert.ToDouble(data);
    public void Add(int key, Vector3 data)
        => vectors[key] = new Vector3Serializable(data);
    public void Add(int key, Vector2 data)
        => vectors[key] = new Vector3Serializable(data);
    public void Add<T>(int key, T data) where T: class
        => objects[key] = data;
        
    public bool TryGet<T>(int key, out T value) where T : class
    {
        value = null!;
        if (!objects.TryGetValue(key, out object data))
            return false;

        if (data is not T tData)
            return false;

        value = tData;
        return true;
    }

    public bool TryGet(int key, out bool value)
    {
        var result = values.TryGetValue(key, out var valueTemp);
        value = Convert.ToBoolean(valueTemp);
        return result;
    }
        
    public bool TryGet(int key, out int value)
    {
        var result = values.TryGetValue(key, out var valueTemp);
        value = Convert.ToInt32(valueTemp);
        return result;
    }
        
    public bool TryGet(int key, out float value)
    {
        var result = values.TryGetValue(key, out var valueTemp);
        value = Convert.ToSingle(valueTemp);
        return result;
    }

    public bool TryGet(int key, out Vector2 value)
    {
        var result = vectors.TryGetValue(key, out var valueTemp);
        value = valueTemp.Convert.ToVector2();
        return result;
    }

    public bool TryGet(int key, out Vector3 value)
    {
        var result = vectors.TryGetValue(key, out var valueTemp);
        value = valueTemp.Convert;
        return result;
    }

    public bool GetBool(int key)
        => Convert.ToBoolean(values[key]);
    public int GetInt(int key)
        => Convert.ToInt32(values[key]);
    public float GetFloat(int key)
        => Convert.ToSingle(values[key]);
    public Vector3 GetVector3(int key)
        => vectors[key].Convert;
    public Vector2 GetVector2(int key)
        => vectors[key].Convert.ToVector2();

    public T Get<T>(int key) where T : class
    {
        if (!TryGet(key, out T value))
            throw new KeyNotFoundException($"{nameof(DataCollection)}: key '{key}' of type '{typeof(T)}' not found.");

        return value;
    }
        
    public void Remove(int key)
    {
        if (objects.ContainsKey(key)) objects.Remove(key);
        if (vectors.ContainsKey(key)) vectors.Remove(key);
        if (values.ContainsKey(key)) values.Remove(key);
    }

    public void Clear()
    {
        objects.Clear();
        vectors.Clear();
        values.Clear();
    }

    public bool IsEmpty => objects.Count == 0 && vectors.Count == 0 && values.Count == 0;

    public bool Equals(IDataCollection other)
        => other is DataCollection dataCollection && Equals(dataCollection);

    public bool Equals(DataCollection other)
        => objects.Equals(other.objects) && values.Equals(other.values) && vectors.Equals(other.vectors);

    public override string ToString()
        => $"Collection({CollectionString(objects)}|{CollectionString(values)}|{CollectionString(vectors)})";

    private static string CollectionString<T>(Dictionary<int, T> collection)
        => string.Join(", ", collection.Select(a => $"\"{a.Key}\": {a.Value}"));
}