﻿using System;
using UnityEngine;

namespace Utilities;

internal sealed class NullDataCollection : IDataCollection, IEquatable<IDataCollection>, IEquatable<DataCollection>
{
    public void Add(int key, int data)
        => throw new System.InvalidOperationException();

    public void Add(int key, float data)
        => throw new System.InvalidOperationException();

    public void Add(int key, bool data)
        => throw new System.InvalidOperationException();

    public void Add(int key, Vector3 data)
        => throw new System.InvalidOperationException();

    public void Add(int key, Vector2 data)
        => throw new System.InvalidOperationException();

    public void Add<T>(int key, T data) where T : class
        => throw new System.InvalidOperationException();

    public bool TryGet(int key, out int value)
        => throw new System.InvalidOperationException();

    public bool TryGet(int key, out float value)
        => throw new System.InvalidOperationException();

    public bool TryGet(int key, out bool value)
        => throw new System.InvalidOperationException();

    public bool TryGet(int key, out Vector3 value)
        => throw new System.InvalidOperationException();

    public bool TryGet(int key, out Vector2 value)
        => throw new System.InvalidOperationException();

    public bool TryGet<T>(int key, out T value) where T : class
        => throw new System.InvalidOperationException();

    public int GetInt(int key)
        => throw new System.InvalidOperationException();

    public float GetFloat(int key)
        => throw new System.InvalidOperationException();

    public bool GetBool(int key)
        => throw new System.InvalidOperationException();

    public Vector3 GetVector3(int key)
        => throw new System.InvalidOperationException();

    public Vector2 GetVector2(int key)
        => throw new System.InvalidOperationException();

    public T Get<T>(int key) where T : class
        => throw new System.InvalidOperationException();

    public void Remove(int key)
        => throw new System.InvalidOperationException();

    public void Clear()
        => throw new System.InvalidOperationException();

    public bool IsEmpty => true;

    public bool Equals(IDataCollection other)
        => other is NullDataCollection;

    public bool Equals(DataCollection other)
        => false;

    public override string ToString()
        => "Null data collection";
}