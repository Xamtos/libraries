﻿using System;
using System.Collections.Generic;

namespace Utilities;

/// <summary>
/// Immutable DTO for list
/// </summary>
public readonly struct ImmutableCollection<T>
{
    private readonly List<T>? list;

    public ImmutableCollection(List<T> list)
    {
        this.list = list ?? throw new NullReferenceException($"{nameof(ImmutableCollection<T>)}: source collection is null.");
    }

    public List<T>.Enumerator GetEnumerator()
        => list?.GetEnumerator() ?? new List<T>.Enumerator();

    public int Count => list?.Count ?? 0;
    public T this[int index] => list![index];
        
    public override string ToString()
        => $"Collection({(list != null ? string.Join("|", list) : string.Empty)})";
}

public static class ImmutableCollectionExtensions
{
    public static bool Contains<T>(this ImmutableCollection<T> source, T value) where T : IEquatable<T>
    {
        foreach (var element in source)
        {
            if (element.Equals(value)) return true;
        }

        return false;
    }
    
    public static bool EqualTo<T>(this ImmutableCollection<T> source, ImmutableCollection<T> target, bool checkOrdering = false) where T : IEquatable<T>
    {
        if (source.Count != target.Count) return false;

        if (checkOrdering)
        {
            for (int i = 0; i < source.Count; i++)
                if (!source[i].Equals(target[i]))
                    return false;
        }
        else
        {
            foreach (var element in source)
                if (!target.Contains(element))
                    return false;
        }

        return true;
    }
}