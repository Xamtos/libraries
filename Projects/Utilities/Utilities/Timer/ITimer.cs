﻿using System;
using JetBrains.Annotations;

namespace Utilities.Timer;

/// <summary>
/// Simple timer to track time intervals affected by timescale.
/// </summary>
[PublicAPI]
public interface ITimer
{
    /// <summary>
    /// Time elapsed since Timer was created or reset.
    /// </summary>
    TimeSpan ElapsedTime { get; }
        
    /// <summary>
    /// Time elapsed since Timer was created or reset in seconds.
    /// </summary>
    float ElapsedSeconds { get; }
        
    /// <summary>
    /// Checks if provided time is elapsed.
    /// </summary>
    bool IsElapsed { get; }
        
    /// <summary>
    /// Time remaining before elapsing.
    /// </summary>
    TimeSpan RemainingTime { get; }
        
    /// <summary>
    /// IsElapsed check and subsequent timer reset if result is true.
    /// </summary>
    bool ResetIfElapsed();
        
    /// <summary>
    /// Reset elapsed time to zero.
    /// </summary>
    void Reset();
}