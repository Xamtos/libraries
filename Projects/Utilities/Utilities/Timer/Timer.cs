﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Timer;

/// <inheritdoc />
[PublicAPI]
public class Timer : ITimer
{
    private const float Tolerance = 0.01f;

    private readonly TimeSpan threshold;
    private float originSeconds;

    /// <summary>
    /// Create a timer with specified interval to track.
    /// </summary>
    public Timer(TimeSpan timeSpan)
    {
        originSeconds = Time.time;
        threshold = timeSpan;
    }

    /// <inheritdoc />
    public TimeSpan ElapsedTime => TimeSpan.FromSeconds(ElapsedSeconds);

    /// <inheritdoc />
    public float ElapsedSeconds => Time.time - originSeconds;

    /// <inheritdoc />
    public bool IsElapsed => threshold < ElapsedTime;
        
    /// <inheritdoc />
    public TimeSpan RemainingTime
    {
        get
        {
            TimeSpan result = threshold - ElapsedTime;
            return result.TotalSeconds < 0 ? TimeSpan.Zero : result;
        }
    }

    /// <inheritdoc />
    public bool ResetIfElapsed()
    {
        if (!IsElapsed) return false;

        Reset();
        return true;
    }
        
    /// <inheritdoc />
    public void Reset()
        => originSeconds = Time.time;
}