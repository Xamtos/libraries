using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Utilities.Extensions;

namespace Utilities;

public static class IdGenerator
{
    private static readonly IDictionary<int, string> Names = new Dictionary<int, string>();
    private static readonly HashAlgorithm HashAlgorithm = HashAlgorithm.Create();

    public static int NameToId(string name)
    {
        if (name.IsNullOrEmpty()) throw new ArgumentException($"{nameof(IdGenerator)}: {nameof(name)} is null or empty.");
        var data = Encoding.ASCII.GetBytes(name);
        var hash = HashAlgorithm.ComputeHash(data);
        var id = BitConverter.ToInt32(hash, 0);
        Names[id] = name;
        return id;
    }

    public static string IdToName(int id)
        => Names.TryGetValue(id, out var name) ? name : id.ToString();
}