﻿using System;
using System.Runtime.Serialization;

namespace Utilities;

[Serializable]
[DataContract]
public class CallbackValue<T> : ICallbackValue<T> where T : IEquatable<T>
{
    [DataMember]
    private T value;
        
    public CallbackValue(T value)
        => this.value = value;

    [field: NonSerialized]
    public event Action<T, T>? OnChangeIncremental;
    [field: NonSerialized]
    public event Action<T>? OnChange;

    public T Value
    {
        get => value;
        set
        {
            if (this.value.Equals(value)) return;

            var old = this.value;
            this.value = value;
            OnChange?.Invoke(value);
            OnChangeIncremental?.Invoke(old, value);
        }
    }

    public void Dispose()
    {
        OnChange = null!;
        OnChangeIncremental = null!;
    }
}

public interface ICallbackValue<T> : ICallbackValueReadonly<T> where T : IEquatable<T>
{
    public new T Value { get; set; }
}
    
public interface ICallbackValueReadonly<out T> : IDisposable where T : IEquatable<T>
{
    public T Value { get; }
    event Action<T, T> OnChangeIncremental;
    event Action<T> OnChange;
}