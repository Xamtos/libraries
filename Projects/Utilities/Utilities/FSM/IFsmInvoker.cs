﻿using System;

namespace Utilities.FSM;

/// <summary>
/// Finite State Machine used to invoke specified action on state change.
/// </summary>
public interface IFsmInvoker<T> : IDisposable where T : Enum
{
    T State { get; }
        
    /// <summary>
    /// Add action to existing transition or create a new one.
    /// </summary>
    void AddTransition(T from, T to, Action? onTransit = null);
        
    /// <summary> Invoke action if state transition was successful. Transition is possible to different state only.</summary>
    /// <param name="newState"> State FSM will attempt to transit to. </param>
    /// <returns> Transition success. </returns>
    bool TryTransitToState(T newState);
        
    /// <summary>
    /// Check if FSM current state equals provided one.
    /// </summary>
    bool IsState(T check);
}