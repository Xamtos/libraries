﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;

namespace Utilities.FSM;

/// <summary>
/// Transition wrapper for specified enum.
/// </summary>
internal readonly struct Transition<T> : IEquatable<Transition<T>> where T : Enum
{
    public Transition(T from, T to)
    {
        if (EqualityComparer<T>.Default.Equals(from, to)) throw new ArgumentException("Cannot create transition to the same state.");
            
        this.from = from;
        this.to = to;
    }

    private readonly T from;
    private readonly T to;
        
    public bool Equals(Transition<T> other)
        => EqualityComparer<T>.Default.Equals(from, other.from) && EqualityComparer<T>.Default.Equals(to, other.to);

    public override bool Equals(object obj)
        => obj is Transition<T> transition && transition.Equals(this);

    public override int GetHashCode()
        => EqualityComparer<T>.Default.GetHashCode(from).CombineHashCode(EqualityComparer<T>.Default.GetHashCode(to));

    public override string ToString()
        => $"Transition({from}|{to})";
}