﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Utilities.FSM;

/// <summary>
/// Class to invoke specified action on state change.
/// </summary>
[PublicAPI]
public class FsmInvoker<T> : IFsmInvoker<T> where T : Enum
{
    private readonly Dictionary<Transition<T>, Action?> transitions = new Dictionary<Transition<T>, Action?>();
        
    public T State { get; private set; }
        
    /// <summary>
    /// Create new FSM with specified starting state.
    /// </summary>
    /// <exception cref="ArgumentException"></exception>
    public FsmInvoker(T state, bool addEmptyTransitions = false)
    {
        var values = (T[]) Enum.GetValues(typeof(T));
        if (values.Length <= 1) throw new ArgumentException($"{typeof(T)} must contain at least two elements to use {nameof(FsmInvoker<T>)} with it.");
        if (!values.Contains(state)) throw new ArgumentException($"{nameof(state)}");

        State = state;

        if (addEmptyTransitions) AddEmptyTransitions(values);
    }

    private void AddEmptyTransitions(IReadOnlyList<T> values)
    {
        for (int i = 0; i < values.Count; i++)
        for (int j = i + 1; j < values.Count; j++)
        {
            AddTransition(values[i], values[j]);
            AddTransition(values[j], values[i]);
        }
    }

    /// <inheritdoc />
    public void AddTransition(T from, T to, Action? onTransit = null)
    {
        var transition = new Transition<T>(from, to);
        if (transitions.TryGetValue(transition, out Action? action)) onTransit += action;

        transitions[transition] = onTransit;
    }

    /// <inheritdoc />
    public bool TryTransitToState(T newState)
    {
        if (IsState(newState)) return false;

        var transition = new Transition<T>(State, newState);
        if (!transitions.TryGetValue(transition, out Action? value)) return false;

        State = newState;
        value?.Invoke();
        return true;
    }
        
    /// <inheritdoc />
    public bool IsState(T check)
        => Compare(State, check);

    private bool Compare(T val1, T val2)
        => EqualityComparer<T>.Default.Equals(val1, val2);

    public void Dispose()
        => transitions.Clear();

    public override string ToString()
        => $"FSM({string.Join(",", transitions.Select(a => a.Key))})";
}