﻿using UnityEngine;

namespace Utilities.MonoBehaviours;

public class ParticlesColorOverTime : MonoBehaviour
{
    [SerializeField] private Gradient gradient = null!;
    [SerializeField] private ParticleSystem particles = null!;
    private float time;

    private void OnEnable()
    {
        time = 0;
    }
        
    private void OnDisable()
    {
        time = 0;
    }

    private void Update()
    {
        time += Time.deltaTime;
        var particlesMain = particles.main;
        particlesMain.startColor = gradient.Evaluate(time);
    }
}