﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Timer;

namespace Utilities.MonoBehaviours;

public class FpsCounter : MonoBehaviour
{
    [SerializeField] private float refreshTime = 0.5f;
    [SerializeField] private bool smoothFps = true;

    private const int SmoothFpsSamplesCount = 10;
    private int frameCounter;
    private ITimer timer;
    private string text;
    private List<int> lastFrames = new List<int>(SmoothFpsSamplesCount);
    
    private void Awake()
    {
        timer = new Timer.Timer(TimeSpan.FromSeconds(refreshTime));
    }

    private void Update()
    {
        var seconds = timer.ElapsedSeconds;
        if (!timer.ResetIfElapsed())
            frameCounter++;
        else
        {
            if (smoothFps)
            {
                lastFrames.Add(frameCounter);
                text = (Avg() / seconds).ToString("0");
                if (lastFrames.Count > SmoothFpsSamplesCount)
                    lastFrames.RemoveAt(0);
            }
            else
            {
                text = (frameCounter / seconds).ToString("0");
            }
            frameCounter = 0;
        }
    }

    private float Avg()
    {
        float current = 0;
        int num = 1;
        foreach (var lastFrame in lastFrames)
        {
            current += lastFrame;
            ++num;
        }
        return current / num;
    }

    private void OnGUI()
        => GUI.Label(new Rect(10, 10, 100, 20), text);
}