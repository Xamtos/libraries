﻿using System.Collections.Generic;
using ECS.Core;
using ECS.Data;
using Identifiers.Counters;
using Utilities;

namespace Components;

public class CountersComponent : Component, ICountersComponent
{
    public ICounters Counters => counters;
    private readonly Counters counters;

    public CountersComponent()
    {
        counters = new Counters();
    }

    public CountersComponent(CounterSerialized[] source)
    {
        counters = new Counters(source);
    }
        
    public void Save(ref IDataCollection data)
    {
        var saveData = new List<CounterSerialized>();
        foreach (var counter in counters) saveData.Add(new CounterSerialized { Id = counter.Key, Value = counter.Value.Value });
        data.Add(IdGenerator.NameToId("CountersSaveData"), saveData);
    }

    public void Load(ref IDataCollection data)
    {
        var saveData = data.Get<List<CounterSerialized>>(IdGenerator.NameToId("CountersSaveData"));
        foreach (var counter in saveData) counters.Add(new Counter(counter.Id, counter.Value));
    }

    public override void Dispose()
    {
        counters.Dispose();
        base.Dispose();
    }
}
    
public interface ICountersComponent : IComponent, IHaveCounters, ISaveable
{
}