﻿using ECS.Core;
using ECS.Data;
using ECS.Init;
using Utilities;

namespace Components;

public class TransientDataComponent : Component, ITransientDataComponent
{
    public IDataCollection TransientData => transientData;
    private DataCollection transientData;

    public TransientDataComponent()
    {
        transientData = new DataCollection();
    }

    public void Init()
        => transientData.Add(IdMap.Owner, Owner);

    public override void Dispose()
    {
        transientData.Clear();
        base.Dispose();
    }

    public void Save(ref IDataCollection data)
        => data.Add(IdGenerator.NameToId("TransientDataSaveData"), transientData);

    public void Load(ref IDataCollection data)
        => transientData = data.Get<DataCollection>(IdGenerator.NameToId("TransientDataSaveData"));
}

public interface ITransientDataComponent : IComponent, IInit, IHaveData, ISaveable
{
    
}