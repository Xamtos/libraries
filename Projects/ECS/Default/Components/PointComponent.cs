﻿using ECS.Core;
using ECS.Data;
using UnityEngine;
using Utilities;
using Component = ECS.Core.Component;

namespace Components;

public class PointComponent : Component, IPointComponent
{
    private readonly CallbackValue<WorldPos> worldPos;

    public ICallbackValueReadonly<WorldPos> WorldPos => worldPos;

    public PointComponent()
    {
        worldPos = new CallbackValue<WorldPos>(Utilities.WorldPos.Default);
    }
        
    public void Translate(Vector3 direction)
    {
        worldPos.Value = new WorldPos(WorldPos.Value.Position + direction, WorldPos.Value.Rotation);
    }

    public void MoveTo(Vector3 position, float speed)
    {
        var remainingDirection = position - WorldPos.Value.Position;
        var direction = remainingDirection.normalized * speed;
        if (remainingDirection.sqrMagnitude < direction.sqrMagnitude) direction = remainingDirection;
        worldPos.Value = new WorldPos(WorldPos.Value.Position + direction, WorldPos.Value.Rotation);
    }

    public void Warp(Vector3 position)
        => worldPos.Value = new WorldPos(position, worldPos.Value.Rotation);

    public void Warp(WorldPos position)
        => worldPos.Value = position;

    public void LerpRotation(Quaternion rotation, float speed)
    {
        rotation = Quaternion.Lerp(WorldPos.Value.Rotation, rotation, speed);
        worldPos.Value = new WorldPos(WorldPos.Value.Position, rotation);
    }

    public void Save(ref IDataCollection data)
    {
        data.Add(IdGenerator.NameToId("PointSaveData.Position"), worldPos.Value.Position);
        data.Add(IdGenerator.NameToId("PointSaveData.Rotation"), worldPos.Value.Rotation.eulerAngles);
    }

    public void Load(ref IDataCollection data)
    {
        var position = data.GetVector3(IdGenerator.NameToId("PointSaveData.Position"));
        var eulerAngles = data.GetVector3(IdGenerator.NameToId("PointSaveData.Rotation"));
        worldPos.Value = new WorldPos(position, Quaternion.Euler(eulerAngles));
    }

    public override void Dispose()
    {
        base.Dispose();
        WorldPos.Dispose();
    }
}

public interface IPointComponent : IComponent, IHavePositionReactive, ISaveable
{
    void Translate(Vector3 direction);
    void MoveTo(Vector3 position, float speed);
    void Warp(Vector3 position);
    void Warp(WorldPos position);
    void LerpRotation(Quaternion rotation, float speed);
}