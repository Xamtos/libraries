﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ECS.Core;

public readonly struct Mask
{
    private readonly int mask01;
    private readonly int mask02;
    private readonly int mask03;
    private readonly int mask04;

    public int Count { get; }

    public Mask() { }
    
    public Mask(int id)
    {
        if (id == 0) throw new ArgumentException("[ComponentsMask.ctor]: received zero id");
        mask01 = id;
        Count = 1;
    }

    public Mask(int id1, int id2) : this(id1)
    {
        if (id2 == id1) throw new ArgumentException("[ComponentsMask.ctor]: received duplicate ids");
        if (id2 == 0) throw new ArgumentException("[ComponentsMask.ctor]: received zero id");
        mask02 = id2;
        Count = 2;
    }

    public Mask(int id1, int id2, int id3) : this(id1, id2)
    {
        if (id3 == id1 || id3 == id2) throw new ArgumentException("[ComponentsMask.ctor]: received duplicate ids");
        if (id3 == 0) throw new ArgumentException("[ComponentsMask.ctor]: received zero id");
        mask03 = id3;
        Count = 3;
    }

    public Mask(int id1, int id2, int id3, int id4) : this(id1, id2, id3)
    {
        if (id4 == id1 || id4 == id2 || id4 == id3) throw new ArgumentException("[ComponentsMask.ctor]: received duplicate ids");
        if (id4 == 0) throw new ArgumentException("[ComponentsMask.ctor]: received zero id");
        mask04 = id4;
        Count = 4;
    }

    public int this[int index] => index switch
    {
        0 => mask01,
        1 => mask02,
        2 => mask03,
        3 => mask04,
        _ => throw new ArgumentOutOfRangeException()
    };

    public bool Has(int id)
    {
        foreach (int mask in this)
            if (mask == id)
                return true;

        return false;
    }

    public bool Any(Mask mask)
    {
        foreach (int id in mask)
            if (Has(id))
                return true;

        return false;
    }

    public bool All(Mask mask)
    {
        foreach (int id in mask)
            if (!Has(id))
                return false;

        return true;
    }

    public bool Equals(Mask other)
    {
        foreach (int tag in other)
        {
            if (!Has(tag))
                return false;
        }

        return Count == other.Count;
    }

    public MaskEnumerator GetEnumerator()
        => new(this);

    public override string ToString()
        => $"Mask({string.Join("|", this)})";

    public bool IsEmpty => Count == 0;
}

public struct MaskEnumerator : IEnumerator<int>
{
    private int index;
    private int current;
    private Mask mask;

    internal MaskEnumerator(Mask mask)
    {
        index = 0;
        current = default;
        this.mask = mask;
        if (this.mask.Count == 0) throw new InvalidOperationException();
    }

    public bool MoveNext()
    {
        while (index < mask.Count)
        {
            if (mask[index] == 0) break;

            current = mask[index];
            ++index;
            return true;
        }

        index = 0;
        current = default;
        return false;
    }


    public int Current => current;
    object IEnumerator.Current => Current;

    void IEnumerator.Reset()
    {
        index = 0;
        current = default;
    }

    public void Dispose()
    {

    }
}