﻿namespace ECS.Core;

public interface IComponentFactory
{
    abstract IComponent Create(int id);
}