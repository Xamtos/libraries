﻿using System;
using ECS.Core.Extensions;
using Identifiers;

namespace ECS.Core;

public abstract class Component : IComponent, ISetOwner
{
    public IEntity Owner { get; set; }
    public int Id { get; }

    protected Component()
    {
        Id = this.GenerateId();
    }

    public virtual void Dispose()
    {
        Owner = null;
    }

    public override string ToString()
        => $"Component({GetType().Name}|{Id})";
}

public interface IComponent : IDisposable, IIdentified
{
    IEntity Owner { get; }
}

public interface ISetOwner
{
    IEntity Owner { set; }
}