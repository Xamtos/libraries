﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECS.Events;
using ECS.Init;
using Managers;
using Utilities.Extensions;

namespace ECS.Core;

public class Systems : ISystems
{
    private bool isInited;
    private readonly HashSet<ISystem> systems = new HashSet<ISystem>();

    private void RegisterSystems()
    {
        foreach (var system in systems)
            Services.Get<IEventRegistrator>().Process(system, true);
    }
    
    public async Task InitAsync()
    {
        if (isInited) throw new InvalidOperationException("Systems are already inited");
        isInited = true;

        RegisterSystems();
        foreach (var system in systems)
        {
            if (system is IInit init)
                init.Init();
        }
        
        var operations = systems.OfType<IInitAsync>().Select(a => a.InitAsync());
        await Task.WhenAll(operations);
    }

    public void AfterInit()
    {
        foreach (var system in systems)
        {
            if (system is IAfterInit afterInit)
                afterInit.AfterInit();
        }
    }

    public async Task Add(ISystem system)
    {
        if (!systems.Add(system))
            throw new InvalidOperationException($"Error in Systems.Add: system already added {nameof(system)}");

        if (isInited)
        {
            if (system is IInit init) init.Init();
            if (system is IInitAsync initAsync) await initAsync.InitAsync();
            if (system is IAfterInit afterInit) afterInit.AfterInit();
        }
    }

    private void Remove(ISystem system)
    {
        if (!systems.Remove(system))
            throw new InvalidOperationException($"Error in Systems.Remove: system does not exist {nameof(system)}");

        if (isInited)
            Services.Get<IEventRegistrator>().Process(system, false);
        if (system is IDisposable disposable) disposable.Dispose();
    }

    public void Remove<T>() where T : ISystem
        => Remove(systems.SingleOfType<T>());

    public void Dispose()
    {
        foreach (ISystem system in systems)
        {
            if (isInited)
                Services.Get<IEventRegistrator>().Process(system, false);
            if (system is IDisposable disposable) disposable.Dispose();
        }
        systems.Clear();
    }
}

public interface ISystems : IDisposable, IAfterInit, IInitAsync
{
    Task Add(ISystem system);
    void Remove<T>() where T : ISystem;
}