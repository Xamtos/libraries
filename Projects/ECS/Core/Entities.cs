using System;
using System.Collections.Generic;
using ECS.Events;
using Managers;
using Managers.EventAggregator;

namespace ECS.Core;

public class Entities : IEntities
{
    public HashSet<IEntity> Collection { get; } = new HashSet<IEntity>();

    public Entities()
    {
        Services.Get<IEventRegistrator>().Process(this, true);
    }

    public void OnTrigger(EntityEvent message)
    {
        bool operationResult = message.State 
            ? Collection.Add(message.Entity) 
            : Collection.Remove(message.Entity);

        if (!operationResult)
            throw new InvalidOperationException($"Error in Entities.OnTrigger with parameters: {message.State}, {message.Entity}");
    }
}

public interface IEntities : IListener<EntityEvent>
{
    HashSet<IEntity> Collection { get; }
}