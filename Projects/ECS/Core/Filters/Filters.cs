﻿using System.Collections.Generic;

namespace ECS.Core;

public static class Filters
{
    private static readonly List<Filter> FiltersCache = new List<Filter>();
        
    public static Filter Get(int include)
    {
        foreach (var filter in FiltersCache)
        {
            if (filter.Include.Count != 1 || !filter.Include.Has(include)) continue;
            if (filter.Exclude.Count > 0) continue;

            return filter;
        }
            
        var newFilter = new Filter(include);
        FiltersCache.Add(newFilter);
        return newFilter;
    }

    public static Filter Get(int include, int exclude)
    {
        foreach (var filter in FiltersCache)
        {
            if (filter.Include.Count != 1 || !filter.Include.Has(include)) continue;
            if (filter.Exclude.Count != 1 || !filter.Exclude.Has(exclude)) continue;

            return filter;
        }
            
        var newFilter = new Filter(include, exclude);
        FiltersCache.Add(newFilter);
        return newFilter;
    }

    public static Filter Get(Mask include)
    {
        foreach (var filter in FiltersCache)
        {
            if (!filter.Include.Equals(include)) continue;
            if (filter.Exclude.Count > 0) continue;

            return filter;
        }

        var newFilter = new Filter(include);
        FiltersCache.Add(newFilter);
        return newFilter;
    }

    public static Filter Get(Mask include, Mask exclude)
    {
        foreach (var filter in FiltersCache)
        {
            if (!filter.Include.Equals(include)) continue;
            if (!filter.Exclude.Equals(exclude)) continue;

            return filter;
        }

        var newFilter = new Filter(include, exclude);
        FiltersCache.Add(newFilter);
        return newFilter;
    }
}