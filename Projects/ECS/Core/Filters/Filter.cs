﻿using System;
using System.Collections.Generic;
using ECS.Events;
using Managers;
using Managers.EventAggregator;

namespace ECS.Core;

public class Filter : IListener<ComponentEvent>, IDisposable
{
    internal Mask Include => include;
    internal Mask Exclude => exclude;

    private readonly Mask include;
    private readonly Mask exclude;
    private readonly List<IEntity> entitiesList = new();

    internal Filter(Mask include, Mask exclude = default)
    {
        this.include = include;
        if (!exclude.IsEmpty) this.exclude = exclude;
        CollectEntities();
        Services.Get<IMessenger>().Subscribe(this);
    }

    internal Filter(int include, int exclude = 0)
    {
        this.include = new Mask(include);
        if (exclude != 0) this.exclude = new Mask(exclude);
        CollectEntities();
        Services.Get<IMessenger>().Subscribe(this);
    }

    public void Dispose()
    {
        entitiesList.Clear();
        Services.Get<IMessenger>().Unsubscribe(this);
    }

    private void CollectEntities()
    {
        var entities = Services.Get<IEntities>();
        foreach (var entity in entities.Collection) ProcessEntity(entity);
    }

    private void ProcessEntity(IEntity entity)
    {
        if (!entity.ContainsMask(Include) || (!exclude.IsEmpty && entity.ContainsMask(exclude)))
        {
            entitiesList.Remove(entity);
            return;
        }

        if (!entitiesList.Contains(entity))
            entitiesList.Add(entity);
    }
        
    public void OnTrigger(ComponentEvent message)
    {
        var id = message.Component.Id;
        if (!include.Has(id) && (exclude.IsEmpty || !exclude.Has(id))) return;

        ProcessEntity(message.Owner);
    }
        
    public List<IEntity>.Enumerator GetEnumerator()
        => entitiesList.GetEnumerator();
}