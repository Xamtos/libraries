using System;

namespace ECS.Core.Extensions;

public static class EntitiesGetExtensions
{
    public static IEntity GetById(this IEntities source, Guid guid)
        => source.TryGetById(guid, out var entity) ? entity : throw new NullReferenceException($"No entity found with id {guid}");

    public static IEntity Get(this IEntities source, int id)
        => source.TryGet(id, out IEntity result) ? result : throw new NullReferenceException($"No entity found with id {id}");

    public static IEntity Get(this IEntities source, Mask mask)
        => source.TryGet(mask, out IEntity result) ? result : throw new NullReferenceException($"No entity found with mask {string.Join(",", mask)}");

    public static bool TryGet(this IEntities source, int tag, out IEntity result)
    {
        var mask = new Mask(tag);
        foreach (var entity in source.Collection)
        {
            if (!entity.ContainsMask(mask)) continue;

            result = entity;
            return true;
        }

        result = null;
        return false;
    }

    public static bool TryGet(this IEntities source, Mask mask, out IEntity result)
    {
        foreach (var entity in source.Collection)
        {
            if (!entity.ContainsMask(mask)) continue;

            result = entity;
            return true;
        }

        result = null;
        return false;
    }
        
    public static bool TryGetById(this IEntities source, Guid guid, out IEntity result)
    {
        foreach (var entity in source.Collection)
        {
            if (entity.Guid != guid) continue;

            result = entity;
            return true;
        }

        result = null;
        return false;
    }
}