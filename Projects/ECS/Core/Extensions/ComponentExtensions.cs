﻿using Utilities;

namespace ECS.Core.Extensions;

public static class ComponentExtensions
{
    public static int GenerateId<T>(this T component) where T : class, IComponent
        => IdGenerator.NameToId(component.GetType().Name);
}