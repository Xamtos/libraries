﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ECS.Data;
using ECS.Events;
using ECS.Init;
using ECS.Templates;
using Managers;
using Managers.EventAggregator;
using Utilities;

namespace ECS.Core;

[Serializable]
public class Entity : IEntity
{
    public Guid Guid { get; }
    public string Name { get; }

    public IReadOnlyDictionary<int, IComponent> Components => components;
    [NonSerialized] private readonly Dictionary<int, IComponent> components;

    [NonSerialized] private bool initCompleted;
    [NonSerialized] private bool initStarted;

    public bool IsInited => initStarted && initCompleted;

    [field: NonSerialized] public bool IsAlive { get; private set; }

    public bool ContainsMask(Mask mask)
    {
        foreach (var id in mask)
        {
            if (!components.ContainsKey(id)) return false;
        }

        return true;
    }
    
    public Entity(IEntityTemplate template) : this(template.Name)
    {
        foreach (var component in template.Components) 
            AddComponent(component);
    }

    public Entity(string name)
    {
        IsAlive = true;
        Guid = Guid.NewGuid();
        Name = name;
        components = new Dictionary<int, IComponent>();
    }

    #region Serialization
    public Entity(SerializationInfo info, StreamingContext context)
    {
        Guid = (Guid) info.GetValue("Guid", typeof(Guid));
        Name = (string) info.GetValue("Name", typeof(string));
            
        var componentIds = (List<int>) info.GetValue("ComponentIds", typeof(List<int>));
        components = new Dictionary<int, IComponent>(componentIds.Count);
        var componentSaveData = (DataCollection) info.GetValue("ComponentSaveData", typeof(DataCollection));
        var factory = Services.Get<IComponentFactory>();
        foreach (var componentId in componentIds)
        {
            var component = factory.Create(componentId);
            if (component is ISaveable saveable)
            {
                var saveData = componentSaveData.Get<IDataCollection>(component.Id);
                saveable.Load(ref saveData);
            }
            AddComponent(component);
        }
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        var componentSaveData = new DataCollection();
        var componentIds = new List<int>(components.Count);
        foreach (var kvp in components)
        {
            componentIds.Add(kvp.Key);
            if (kvp.Value is not ISaveable saveable) continue;

            IDataCollection saveData = new DataCollection();
            saveable.Save(ref saveData);
            componentSaveData.Add(kvp.Key, saveData);
        }

        info.AddValue("Guid", Guid, typeof(Guid));
        info.AddValue("Name", Name, typeof(string));
        info.AddValue("ComponentSaveData", componentSaveData, typeof(DataCollection));
        info.AddValue("ComponentIds", componentIds, typeof(List<int>));
    }

    #endregion

    public void Init()
        => Init(out _);

    public void Init(out Task initialization)
    {
        if (initStarted) throw new InvalidOperationException($"Entity already inited: {this}");

        initStarted = true;
        foreach (var kvp in components)
        {
            if (kvp.Value is not IInitAsync) continue;

            initialization = InitContinuationAsync();
            return;
        }

        InitContinuation();
        initialization = Task.CompletedTask;
    }

    private void InitContinuation()
    {
        foreach (var kvp in components)
            if (kvp.Value is IInit init)
                init.Init();

        InitCompletion();
    }

    private async Task InitContinuationAsync()
    {
        var tasks = new List<Task>();
        foreach (var kvp in components)
        {
            if (kvp.Value is IInit init) init.Init();
            if (kvp.Value is IInitAsync initAsync) tasks.Add(initAsync.InitAsync());
        }

        await Task.WhenAll(tasks);
        InitCompletion();
    }

    private void InitCompletion()
    {
        foreach (var kvp in components) Services.Get<IMessenger>().Send(new ComponentEvent(this, kvp.Value, true));
        Services.Get<IMessenger>().Send(new EntityEvent(this, true));
        foreach (var kvp in components)
        {
            if (kvp.Value is IAfterInit afterInit) afterInit.AfterInit();
        }

        initCompleted = true;
    }

    public void Dispose()
    {
        Services.Get<IMessenger>().Send(new EntityEvent(this, false));
        foreach (var kvp in components)
        {
            Services.Get<IMessenger>().Send(new ComponentEvent(this, kvp.Value, false));
            kvp.Value.Dispose();
        }

        components.Clear();
        IsAlive = false;
    }

    public override string ToString()
        => $"Entity({Guid}, {Name})";

    public void AddComponent<T>(T component) where T : class, IComponent
    {
        components.Add(component.Id, component);
        ((ISetOwner)component).Owner = this;
        
        if (!IsInited) return;

        if (component is IInit init) init.Init();
        Services.Get<IMessenger>().Send(new ComponentEvent(this, component, true));
        if (component is IAfterInit afterInit)
            afterInit.AfterInit();
    }
        
    public void RemoveComponent(int id)
    {
        if (!components.TryGetValue(id, out var component)) throw new IndexOutOfRangeException();
        
        Services.Get<IMessenger>().Send(new ComponentEvent(this, component, false));
        components.Remove(id);
        component.Dispose();
    }
}

public interface IEntity : IDisposable, ISerializable, IInit
{
    void Init(out Task initialization);
    Guid Guid { get; }
    bool IsInited { get; }
    bool IsAlive { get; }
    IReadOnlyDictionary<int, IComponent> Components { get; }
    bool ContainsMask(Mask include);
    void AddComponent<T>(T component) where T : class, IComponent;
    void RemoveComponent(int id);
}