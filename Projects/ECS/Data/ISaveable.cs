﻿using Utilities;

namespace ECS.Data;

public interface ISaveable
{
    void Save(ref IDataCollection data);
    void Load(ref IDataCollection data);
}