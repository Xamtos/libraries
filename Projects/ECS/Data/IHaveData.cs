﻿using Utilities;

namespace ECS.Data;

public interface IHaveData
{
    IDataCollection TransientData { get; }
}