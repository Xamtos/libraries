﻿using ECS.Core;
using Managers.EventAggregator;

namespace ECS.Events;

public readonly struct EntityEvent : IMessage
{
    public EntityEvent(IEntity entity, bool state)
    {
        Entity = entity;
        State = state;
    }

    public IEntity Entity { get; }
    public bool State { get; }
}