﻿using ECS.Core;
using Managers.EventAggregator;

namespace ECS.Events;

public readonly struct ComponentEvent : IMessage
{
    public ComponentEvent(IEntity owner, IComponent component, bool state)
    {
        Owner = owner;
        Component = component;
        State = state;
    }

    public IEntity Owner { get; }
    public IComponent Component { get; }
    public bool State { get; }

    public bool IsComponent<T>(int id, out T component)
    {
        if (Component.Id != id)
        {
            component = default;
            return false;
        }

        component = (T)Component;
        return true;
    }
}