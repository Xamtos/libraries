﻿namespace ECS.Events;

public interface IEventRegistrator
{
    void Process(object subject, bool state);
}