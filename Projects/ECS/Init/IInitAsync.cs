using System.Threading.Tasks;

namespace ECS.Init;

public interface IInitAsync
{
    Task InitAsync();
}