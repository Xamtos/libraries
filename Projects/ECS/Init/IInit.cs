﻿namespace ECS.Init;

public interface IInit
{
    void Init();
}

public interface IInit<in T>
{
    void Init(T value);
}