namespace ECS.Init;

public interface IAfterInit
{
    void AfterInit();
}