using Identifiers;

namespace ECS.Templates;

public interface IEntityTemplate : ITemplate, IComponentsSource
{
    string Name { get; }
}