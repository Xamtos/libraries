﻿using System.Collections.Generic;
using IComponent = ECS.Core.IComponent;

namespace ECS.Templates;

public interface IComponentsSource
{
    IEnumerable<IComponent> Components { get; }
}